﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface INewsService : IDisposable
    {
        List<NewsViewModel> GetAll();

        NewsViewModel GetById(int id);

        PagedResult<NewsViewModel> GetAllPaging(string keyWord, int page, int pageSize);

        List<NewsViewModel> GetLastestNews(int top);

        NewsViewModel Add(NewsViewModel newsViewModel);

        void Update(NewsViewModel newsViewModel);

        void Delete(int id);
    }
}
