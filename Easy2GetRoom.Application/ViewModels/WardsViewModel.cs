﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Easy2GetRoom.Application.ViewModels
{
    public class WardsViewModel
    {
        public int Id { get; set; }
        public int DistrictId { get; set; }

        public string Name { get; set; }

        public DistrictViewModel District { get; set; }

    }
}
