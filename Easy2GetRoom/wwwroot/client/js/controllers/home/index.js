﻿var homeControlelr = function () {
    this.initialize = function () {    
        registerEvent();

    }
    function registerEvent() {
        $('#dllCity').on('change', function () {
            var that = $(this).val();
            getDistrictByCityId(that);
            $("#dllDistrict").prop("disabled", false);
        });

        $('#dllDistrict').on('change', function () {
            var that = $(this).val();
            getWardsByDistrictId(that);
            $("#dllWards").prop("disabled", false);
        });

        if ($('#dllCity').val() == 0) {
            //$('#dllDistrict').attr('disabled', 'disabled');
            $("#dllDistrict").prop("disabled", true);
        }
        if ($('#dllDistrict').val() == 0) {
            $('#dllWards').attr('disabled', 'disabled');
        }
    };

    function getDistrictByCityId(id) {
        $('#dllDistrict').html('');
        $.ajax({
            type: 'POST',
            url: '/Home/GetgetDistrictbyCityId',
            data: {
                'id': id
            },
            dataType: 'Json',
            success: function (response) {
                console.log(response);
                var render = "<option value=''>" + "Quận Huyện" + "</option>";
                $.each(response, function (i, item) {
                    render += "<option value = '" + item.Id + "'>" + item.Name + "</option>";
                });

                $('#dllDistrict').html(render);
            },
            error: function (status) {
                console.log(status);
            }
        });
    }

    function getWardsByDistrictId(id) {
        $('#dllWards').html('');
        $.ajax({
            type: 'POST',
            url: '/Home/GetWardsByDistrictId',
            data: {
                'id': id
            },
            dataType: 'Json',
            success: function (response) {
                console.log(response);
                var render = "<option value=''>" + "Phường xã, Thị trấn" + "</option>";
                $.each(response, function (i, item) {
                    render += "<option value = '" + item.Id + "'>" + item.Name + "</option>";
                });

                console.log(render);

                $('#dllWards').html(render);
            },
            error: function (status) {
                console.log(status);
            }
        });
    }
}