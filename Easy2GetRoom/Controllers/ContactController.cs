﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Extensions;
using Easy2GetRoom.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Easy2GetRoom.Controllers
{
    [Authorize]
    public class ContactController : Controller
    {
        private readonly IMessageService _messageService;
        private readonly IUnitOfWork _unitOfWork;

        public ContactController(IMessageService messageService, IUnitOfWork unitOfWork)
        {
            _messageService = messageService;
            _unitOfWork = unitOfWork;
        }

        [Route("contact.html")]
        public IActionResult Index()
        {
            return View();
        }

        #region AJAX API

        public IActionResult SaveEntity(MessageViewModel messageViewModel)
        {
            if (!string.IsNullOrEmpty(messageViewModel.EmailSender))
            {
                messageViewModel.IdSender = Guid.Parse(User.GetSpecificClaim("Id"));
                messageViewModel.IsReply = ReplyMessage.NotReplied;
                return new OkObjectResult(_messageService.Add(messageViewModel));
            }
            return new BadRequestResult();
        }

        public IActionResult UpdateStatus(int id, ReplyMessage isReply)
        {
            _messageService.UpdateStatus(id, isReply);
            _unitOfWork.Commit();
            return new OkResult();
        }

        #endregion AJAX API
    }
}