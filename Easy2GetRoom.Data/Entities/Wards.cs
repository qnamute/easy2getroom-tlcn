﻿using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("Wards")]
    public class Wards: DomainEntity<int>
    {
        public Wards()
        {
            Properties = new HashSet<Property>();
        }

        public Wards(int id, int districtId, string name)
        {
            Id = id;
            DistrictId = districtId;
            Name = name;
        }


        public int DistrictId { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [ForeignKey("DistrictId")]
        public virtual District District { get; set; }

        public virtual ICollection<Property> Properties { get; set; }
    }
}
