﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Easy2GetRoom.Data.EF.Migrations
{
    public partial class initial07 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentId",
                table: "Messages",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "Messages");
        }
    }
}
