﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces.Client;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Application.Implementations.Client
{
    public class UserManagePostService : IUserManagePostService
    {
        private readonly IPropertyRepository _propertyRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserManagePostService(IPropertyRepository propertyRepository, IUnitOfWork unitOfWork)
        {
            _propertyRepository = propertyRepository;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _propertyRepository.Remove(id);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<PropertyViewModel> GetAllActivePropery(string userId)
        {
            var query = _propertyRepository.FindAll(x=>x.Status == Status.Active);
            if (!string.IsNullOrEmpty(userId))
            {
                query = query.Where(x => x.UserId == Guid.Parse(userId));
            }
            return query.ProjectTo<PropertyViewModel>().ToList();
        }

        public List<PropertyViewModel> GetAllAwaitingApprovalPropery(string userId)
        {
            var query = _propertyRepository.FindAll(x => x.Status == Status.AwaitingApproval);
            if (!string.IsNullOrEmpty(userId))
            {
                query = query.Where(x => x.UserId == Guid.Parse(userId));
            }
            return query.ProjectTo<PropertyViewModel>().ToList();
        }

        public List<PropertyViewModel> GetAllInActivePropery(string userId)
        {
            var query = _propertyRepository.FindAll(x => x.Status == Status.InActive);
            if (!string.IsNullOrEmpty(userId))
            {
                query = query.Where(x => x.UserId == Guid.Parse(userId));
            }
            return query.ProjectTo<PropertyViewModel>().ToList();
        }

        public void UpdateStatus(PropertyViewModel propertyViewModel)
        {
            _propertyRepository.Update(Mapper.Map<PropertyViewModel, Property>(propertyViewModel));
        }
    }
}