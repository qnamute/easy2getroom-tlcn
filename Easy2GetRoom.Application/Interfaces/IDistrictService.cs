﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IDistrictService : IDisposable
    {
        List<DistrictViewModel> GetAll();

        DistrictViewModel GetById(int id);

        void UpdateStatus(int id, Status status);

        void Delete(int id);

        void Update(DistrictViewModel districtViewModel);

        void Save();

        List<DistrictViewModel> GetDistrictbyCityId(int id);
    }
}
