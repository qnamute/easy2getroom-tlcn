﻿using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("Cities")]
    public class City: DomainEntity<int>
    {
        public City()
        {
            Districts = new HashSet<District>();
            Properties = new HashSet<Property>();
        }
        public City(string name)
        {
            Name = name;
        }
        public City (int id, string name)
        {
            Id = id;
            Name = name;
        }


        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        public virtual ICollection<Property> Properties { get; set; }

        public virtual ICollection<District> Districts { get; set; }
    }
}
