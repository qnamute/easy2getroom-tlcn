﻿var upPostController = function () {
    this.initialize = function () {
        registerEvent();
        registerControls();
    }
    function registerEvent() {
        $('#showmodal').on('click', function () {
            console.log('!@#!');
            $('#exampleModal').modal('show');
        });

        // Step form
        $("#wizard1").simpleWizard({
            cssClassStepActive: "active",
            cssClassStepDone: "done"
        });

        //Upload image
        (function () {
            var options = {};
            $('.js-uploader__box').uploader(options);
        }());

        //User config
        //Step 01
        $('.btn-category').on('click', function () {
            $('#txtPropertyCategory').val($(this).data("name"));
            $('#txtPropertyCategoryRequired').val($(this).data("id"));
        });

        //Step 02
        $('.btn-rental-type').on('click', function () {
            $('#txtRentalType').val($(this).data("name"));
            $('#txtRentalTypeRequired').val($(this).data("id"));

            let rentalType = $(this).data("id");
            if (rentalType == commonconstants.rentalType.forRent
                || rentalType == commonconstants.rentalType.forStayWith) {
                onPropertyFor();
            }
            if (rentalType == commonconstants.rentalType.needRent
                || rentalType == commonconstants.rentalType.needStayWith) {
                onPropertyNeed();
            }
        });


        // Step 03
        // Load district by cityid


        if ($('#dllCity').val() == 0) {
            $("#dllDistrict").prop("disabled", true);
        }
        if ($('#dllDistrict').val() == 0) {
            $('#dllWards').attr('disabled', 'disabled');
        }

        $('#dllCity').on('change', function () {
            var that = $(this).val();
            getDistrictByCityId(that);
            $("#dllDistrict").prop("disabled", false);
        });

        $('#dllDistrict').on('change', function () {
            var that = $(this).val();
            getWardsByDistrictId(that);
            $("#dllWards").prop("disabled", false);
        });

        $('#fileinput').on('change', function () {
            console.log($(this).val());
        });

        // Step 05

        $('.acreage-val').on('change', function () {
            var suggestTitle = getSuggestTitle();
            $('#btnSuggestTitle').data("title", suggestTitle);
            $('#btnSuggestTitle').val(suggestTitle);
        });

        // Step 06
        $('#btnSuggestTitle').on('click', function () {
            $('#txtTitle').val($(this).data('title'));
        });

        // save post
        $('#btnUpostConfirm').on('click', function () {
            getLatLngFromAddress();
        });
        // 
    }

    function getLatLngFromAddress() {
        let address = $('#txtApartmentNumber').val() + " " + $('#txtStreetName').val() + " " + $('#dllCity option:selected').text();
        console.log(address);
        $.ajax({
            type: 'GET',
            url: 'https://maps.googleapis.com/maps/api/geocode/json',
            data: {
                'address': address,
                'key': 'AIzaSyBEfLTmYpED4HyRSF1KaSyyqZ7m7ZGYxpU'
            },
            success: function (data) {
                console.log(data);
                let location = data.results[0].geometry.location;
                $('#txtLat').val(location.lat);
                $('#txtLng').val(location.lng);
                savePostProperty();
            },
            error: function (status) {
                console.log(status);
            }
        });
    }

    function savePostProperty() {
        let name = $('#txtName').val();
        let category = $('#txtPropertyCategoryRequired').val();
        let rentalType = $('#txtRentalTypeRequired').val();
        let city = $('#dllCity').val();
        let district = $('#dllDistrict').val();
        let wards = $('#dllWards').val();
        let title = $('#txtTitle').val();
        let description = CKEDITOR.instances.txtDescription.getData();
        let price = $('#txtPrice').val();
        let priceFrom = $('#txtPriceFrom').val();
        let priceTo = $('#txtPriceTo').val();
        let acreage = $('#txtAcreage').val();
        let acreageFrom = $('#txtAcreageFrom').val();
        let acreageTo = $('#txtAcreageTo').val();
        let address = $('#txtApartmentNumber').val() + " " + $('#txtStreetName').val();
        let lng = $('#txtLng').val();
        let lat = $('#txtLat').val();
        $.ajax({
            type: 'POST',
            url: '/UpPost/SavePost',
            data: {
                Name: name,
                PropertyCategoryId: category,
                CityId: city,
                DistrictId: district,
                WardsId: wards,
                RentalTypeId: rentalType,
                Title: title,
                Description: description,
                Price: price,
                PriceFrom: priceFrom,
                PriceTo: priceTo,
                Acreage: acreage,
                AcreageFrom: acreageFrom,
                AcreageTo: acreageTo,
                Address: address,
                Lat: lat,
                Lng: lng,
            },
            dataType: 'json',
            success: function (response) {
                console.log(response);
                var propertyId = response.Id;

                var savedImage = savePropertyImages(propertyId);

                if (savedImage != null) {
                    console.log('Up post success !');
                };

                $(location).attr('href', 'https://localhost:44389/uppost-success.html');

            },
            error: function (status) {
                console.log(status);
                $(location).attr('href', 'https://localhost:44389/uppost-failed.html');
            }
        });
    }


    function getSuggestTitle() {
        let category = $('#txtPropertyCategory').val();
        let district = $('#dllDistrict option:selected').text();
        let acreage = $('#txtAcreage').val();
        let acreageFrom = $('#txtAcreageFrom').val();
        let acreageTo = $('#txtAcreageTo').val();

        let suggestTitle = category + ' ' + district + ' diện tích ';
        if (acreage != "") {
            suggestTitle += acreage + ' mét vuông';
        }
        if (acreageFrom != "" && acreageTo != "") {
            suggestTitle += 'từ ' + acreageFrom + ' đến ' + acreageTo + ' mét vuông';
        }
        return suggestTitle;
    }

    function registerControls() {
        CKEDITOR.replace('txtDescription', {

        });

        // Fix ckeditor link
        // Fix: cannot click on element ck in modal
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
            $(document)
                .off('focusin.bs.modal') // guard against infinite focus loop
                .on('focusin.bs.modal', $.proxy(function (e) {
                    if (
                        this.$element[0] !== e.target && !this.$element.has(e.target).length
                        // CKEditor compatibility fix start.
                        && !$(e.target).closest('.cke_dialog, .cke').length
                        // CKEditor compatibility fix end.
                    ) {
                        this.$element.trigger('focus');
                    }
                }, this));
        };
    }

    function savePropertyImages(propertyId) {
        var imagesUrl = new Array();
        $('.images-url').each(function () {
            imagesUrl.push($(this).val());
        });

        $.ajax({
            type: 'POST',
            url: 'UpPost/SaveImageProduct',
            data: {
                imagesUrl: imagesUrl,
                propertyId: propertyId
            },
            dataType: 'json',
            success: function (response) {
                console.log(response);
                return response;
            },
            error: function (status) {
                console.log(status);
            }
        })
    }

    function getDistrictByCityId(id) {
        $('#dllDistrict').html('');
        $.ajax({
            type: 'POST',
            url: '/Home/GetgetDistrictbyCityId',
            data: {
                'id': id
            },
            dataType: 'Json',
            success: function (response) {
                console.log(response);
                var render = "<option value='0'>" + "Quận Huyện" + "</option>";
                $.each(response, function (i, item) {
                    render += "<option value = '" + item.Id + "'>" + item.Name + "</option>";
                });

                $('#dllDistrict').html(render);
            },
            error: function (status) {
                console.log(status);
            }
        });
    }
    function getWardsByDistrictId(id) {
        $('#dllWards').html('');
        $.ajax({
            type: 'POST',
            url: '/Home/GetWardsByDistrictId',
            data: {
                'id': id
            },
            dataType: 'Json',
            success: function (response) {
                console.log(response);
                var render = "<option value='0'>" + "Phường xã, Thị trấn" + "</option>";
                $.each(response, function (i, item) {
                    render += "<option value = '" + item.Id + "'>" + item.Name + "</option>";
                });

                console.log(render);

                $('#dllWards').html(render);
            },
            error: function (status) {
                console.log(status);
            }
        });
    }

    function onPropertyFor() {
        $('.div-for').show();
        $('.div-need').hide();
        $('#txtPrice').addClass('required');
        $('#txtPriceFrom').removeClass('required');
        $('#txtPriceTo').removeClass('required');
        $('#txtAcreage').addClass('required');
        $('#txtAcreageFrom').removeClass('required');
        $('#txtAcreageTo').removeClass('required');
    }

    function onPropertyNeed() {
        $('.div-for').hide();
        $('.div-need').show();
        $('#txtPrice').removeClass('required');
        $('#txtPriceFrom').addClass('required');
        $('#txtPriceTo').addClass('required');
        $('#txtAcreage').removeClass('required');
        $('#txtAcreageFrom').addClass('required');
        $('#txtAcreageTo').addClass('required');
    }
}