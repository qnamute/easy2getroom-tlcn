﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class ActivePropertyController : BaseController
    {
        private IPropertyService _propertyService;
        private IPropertyCategoryService _propertyCategoryService;

        public ActivePropertyController(IPropertyService propertyService, IPropertyCategoryService propertyCategoryService)
        {
            _propertyService = propertyService;
            _propertyCategoryService = propertyCategoryService;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region AJAX API

        [HttpGet]
        public IActionResult GetAllCategories()
        {
            var model = _propertyCategoryService.GetAll();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging(int? categoryId, string keyWord, int page, int pageSize)
        {
            var model = _propertyService.GetAllActivePropertyPaging(categoryId, keyWord, page, pageSize, null);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            var model = _propertyService.GetById(id);
            return new OkObjectResult(model);
        }


        [HttpPost]
        public IActionResult Delete(int id)
        {
            _propertyService.Delete(id);
            _propertyService.Save();

            return new OkResult();
        }

        [HttpPost]
        public IActionResult Hide(int id)
        {
            _propertyService.UpdateStatus(id, Status.InActive);
            _propertyService.Save();
            return new OkResult();
        }

        [HttpPost]
        public IActionResult SaveEntity(PropertyViewModel propertyViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (propertyViewModel.Id == 0)
                {
                    //Add new property
                }
                else
                {
                    //Update property
                    _propertyService.Update(propertyViewModel);
                }
                _propertyService.Save();

                return new OkObjectResult(propertyViewModel);
            }
        }

        #endregion AJAX API
    }
}