﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Data.EF.Repositories
{
    public class NewsImageRepository : EFRepository<NewsImage, int>, INewsImageRepository
    {
        public NewsImageRepository(AppDbContext context) : base(context)
        {
        }
    }
}
