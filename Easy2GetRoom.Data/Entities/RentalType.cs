﻿using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("RentalTypes")]
    public class RentalType : DomainEntity<int>, IDateTracking
    {
        public RentalType()
        {
            Properties = new HashSet<Property>();
        }

        public RentalType(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public RentalType(int id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }



        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [StringLength(1000)]
        [Required]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public virtual ICollection<Property> Properties { get; set; }
    }
}
