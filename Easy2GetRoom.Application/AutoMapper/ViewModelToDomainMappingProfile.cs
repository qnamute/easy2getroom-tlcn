﻿using AutoMapper;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;

namespace Easy2GetRoom.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<PropertyCategoryViewModel, PropertyCategory>()
                .ConstructUsing(c => new PropertyCategory(
                    c.Name, c.Description, c.Status, c.SortOrder));

            CreateMap<FunctionViewModel, Function>()
                .ConstructUsing(c => new Function(c.Id, c.Name, c.URL, c.ParentId, c.IconCss,
                     c.SortOrder));

            CreateMap<PropertyViewModel, Property>()
                .ConstructUsing(c => new Property(c.Id, c.Name, c.PropertyCategoryId, c.CityId, c.DistrictId, c.WardsId, c.RentalTypeId,
                c.Title, c.Description, c.Price, c.PriceFrom, c.PriceTo, c.Acreage, c.AcreageFrom, c.AcreageTo, c.Address,
                c.Lat, c.Lng, c.Status, c.SlideFlag, c.UserId));

            CreateMap<PropertyImageViewModel, PropertyImage>()
                .ConstructUsing(c => new PropertyImage(c.Id, c.Url, c.PropertyId));

            CreateMap<NewsViewModel, News>()
                .ConstructUsing(c => new News(c.Id, c.Title, c.Content, c.UserId, c.DateCreated, c.DateModified));

            CreateMap<NewsImageViewModel, NewsImage>()
                .ConstructUsing(c => new NewsImage(c.Id, c.Url, c.NewsId));

            CreateMap<MessageViewModel, Message>()
                .ConstructUsing(c => new Message(c.Id, c.ParentId, c.IdSender, c.EmailSender, c.IsReply, c.Title, c.Content, c.DateCreated, c.DateModified));
        }
    }
}