﻿using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("News")]
    public class News : DomainEntity<int>, IDateTracking
    {
        public News(string title, string content, Guid userId, DateTime dateCreated, DateTime dateModified)
        {
            Title = title;
            Content = content;
            UserId = userId;
            DateCreated = dateCreated;
            DateModified = dateModified;
        }

        public News(int id, string title, string content, Guid userId, DateTime dateCreated, DateTime dateModified)
        {
            Id = id;
            Title = title;
            Content = content;
            UserId = userId;
            DateCreated = dateCreated;
            DateModified = dateModified;
        }

        public string Title { get; set; }

        [StringLength(10000)]
        [Required]
        public string Content { get; set; }

        public Guid UserId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public virtual ICollection<NewsImage> NewsImages { get; set; }
    }
}
