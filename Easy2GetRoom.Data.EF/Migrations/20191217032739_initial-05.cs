﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Easy2GetRoom.Data.EF.Migrations
{
    public partial class initial05 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Users_IdReceiver",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_IdReceiver",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "IdReceiver",
                table: "Messages");

            migrationBuilder.AddColumn<int>(
                name: "SlideFlag",
                table: "Properties",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Messages",
                maxLength: 200,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SlideFlag",
                table: "Properties");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Messages");

            migrationBuilder.AddColumn<Guid>(
                name: "IdReceiver",
                table: "Messages",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Messages_IdReceiver",
                table: "Messages",
                column: "IdReceiver");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Users_IdReceiver",
                table: "Messages",
                column: "IdReceiver",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
