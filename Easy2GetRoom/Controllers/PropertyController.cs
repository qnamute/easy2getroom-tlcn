﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Models.PropertyViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Easy2GetRoom.Controllers
{
    public class PropertyController : Controller
    {
        private IPropertyService _propertyService;
        private IPropertyCategoryService _propertyCategoryService;
        private IPropertyImageService _propertyImageService;

        private IConfiguration _configuration;

        public PropertyController(IPropertyService propertyService, IPropertyCategoryService propertyCategoryService,
            IPropertyImageService propertyImageService, IConfiguration configuration)
        {
            _propertyService = propertyService;
            _propertyCategoryService = propertyCategoryService;
            _propertyImageService = propertyImageService;
            _configuration = configuration;
        }

        [Route("filter-result.html")]
        public IActionResult Filter(int? categoryId, int? rentalTypeId, int? cityId, int? districtId,
            int? wardsId, int? acreageRange, int? priceRange, int page = 1)
        {
            var catalog = new CatalogViewModel();

            var pageSize = _configuration.GetValue<int>("PageSize");
            catalog.PageSize = pageSize;
            catalog.Data = _propertyService.GetFilterPaging(categoryId, rentalTypeId, cityId, districtId, wardsId, acreageRange,
                priceRange, page, pageSize);
            return View(catalog);
        }

        [Route("search-result.html")]
        public IActionResult Search(string keyword, int? pageSize, string sortBy, int page = 1)
        {
            var catalog = new SearchResultViewModel();

            if (pageSize == null)
            {
                pageSize = _configuration.GetValue<int>("PageSize");
            }
            catalog.PageSize = pageSize;
            catalog.Data = _propertyService.GetAllActivePropertyPaging(null, keyword, page, pageSize.Value, null);
            catalog.Keyword = keyword;

            return View(catalog);
        }

        [Route("easy2getroom-property-c.{id}.html")]
        public IActionResult CataLog(int id, string keyword, int? pageSize, string sortBy, int page = 1)
        {
            var catalog = new CatalogViewModel();

            if (pageSize == null)
            {
                pageSize = _configuration.GetValue<int>("PageSize");
            }

            catalog.Data = _propertyService.GetAllActivePropertyPaging(id, string.Empty, page, pageSize.Value, null);

            return View(catalog);
        }

        [Route("all-properties.html")]
        public IActionResult AllProperties(string keyword, int? pageSize, string sortBy, int page = 1)
        {
            var catalog = new CatalogViewModel();

            if (pageSize == null)
            {
                pageSize = _configuration.GetValue<int>("PageSize");
            }

            catalog.Data = _propertyService.GetAllActivePropertyPaging(null, string.Empty, page, pageSize.Value, null);
            return View(catalog);
        }

        [Route("easy2getroom-property-p.{id}.html", Name = "PropertyDetail")]
        public IActionResult Details(int id)
        {
            var model = new DetailViewModel();
            model.Property = _propertyService.GetById(id);
            model.PropertyImages = _propertyImageService.GetPropertyImageByPropertyId(id);
            model.RelatedProperties = _propertyService.GetRelatedProperties(id, 3);
            return View(model);
        }
    }
}