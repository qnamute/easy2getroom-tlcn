﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy2GetRoom.Models.PropertyViewModels
{
    public class FilterViewModel
    {
        public int? Category { get; set; }

        public int? RentalType { get; set; }

        public int? City { get; set; }

        public int? District { get; set; }

        public int? Wards { get; set; }

        public int? AcreageRange { get; set; }

        public int? PriceRange { get; set; }

        public int? Page { get; set; }

        public int? PageSize { get; set; }
    }
}
