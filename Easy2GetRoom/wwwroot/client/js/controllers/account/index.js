﻿var accountController = function () {
    this.initialize = function () {
        registerEvent();
    }

    function registerEvent() {
        $('#btnChangeAvatar').on('click', function () {
            $('#avatar').trigger('click');
        });

        $('#avatar').on('change', function () {

            var fileUpload = $(this).get(0);
            var files = fileUpload.files;
            console.log(files);
            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                data.append(files[i].name, files[i]);
            }

            $.ajax({
                type: 'POST',
                url: '/Upload/UploadAvatar',
                contentType: false,
                processData: false,
                data: data,
                success: function (path) {
                    console.log('Upload success', path);
                    $('#imgAvatar').attr('src', path);
                    uploadAvatarUser(path);
                },
                error: function (error) {
                    console.log(error);
                }
            })
        });

        // change passsword btn click event
        $('#btnChangePassword').on('click', function () {
            console.log('asdasd');
            $('#changePassword').modal('show');
        });


        // Edit info
        $('#btnEditFullName').on('click', function () {
            $('#txtFullName').prop('disabled', false);
        });
        $('#btnEditPhoneNumber').on('click', function () {
            $('#txtPhoneNumber').prop('disabled', false);
        });
        //$('#btnEditAddress').on('click', function () {
        //    $('#txtAddress').prop('disabled', false);
        //});

        $('#btnSaveChange').on('click', function () {
            updateInfo();
        });
    }

    function uploadAvatarUser(path) {
        $.ajax({
            type: 'POST',
            url: '/ManageAccount/UploadAvatarUser',
            data: {
                'path': path
            },
            success: function () {
                console.log('Upload avatar user success');
            },
            error: function () {
                console.log('Upload avatar user failed');
            }
        }); 
    }

    function updateInfo() {
        let fullName = $('#txtFullName').val();
        let phoneNumber = $('#txtPhoneNumber').val();
        //let address = $('#txtAddress').val();
        $.ajax({
            type: 'POST',
            url: '/ManageAccount/UpdateInfo',
            data: {
                'FullName': fullName,
                'PhoneNumber': phoneNumber,
                //'Address': address
            },
            success: function (result) {
                console.log(result);
                $('#notifyModal').modal('show');
            }
        });
    }
}