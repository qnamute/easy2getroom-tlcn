﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;

namespace Easy2GetRoom.Data.EF.Repositories
{
    public class MessageRepository : EFRepository<Message, int>, IMessageRepository
    {
        public MessageRepository(AppDbContext context) : base(context)
        {
        }
    }
}