﻿namespace Easy2GetRoom.Data.Enums
{
    public enum Status
    {
        InActive,
        Active,
        AwaitingApproval,
    }
}