﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Extensions;
using Easy2GetRoom.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Easy2GetRoom.Controllers
{
    public class ManageAccountController : Controller
    {
        private UserManager<User> _userManager;
        private IUnitOfWork _unitOfWork;
        private IUserService _userService;

        public ManageAccountController(UserManager<User> userManager, IUnitOfWork unitOfWork, IUserService userService)
        {
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        [Route("account.html")]
        public IActionResult Index()
        {
            UserViewModel user = new UserViewModel();

            string userId = User.GetSpecificClaim("Id");

            user = _userService.GetUser(userId);
            
            return View(user);
        }

        #region AJAX API

        [HttpPost]
        public IActionResult UploadAvatarUser(string path)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = _userManager.FindByIdAsync(User.GetSpecificClaim("Id"));
                user.Result.Avatar = path;
                _unitOfWork.Commit();
                return new OkResult();
            }
            else
            {
                return new BadRequestResult();
            }
        }

        public IActionResult UpdateInfo(UserViewModel userViewModel)
        {

            if(User.Identity.IsAuthenticated)
            {
                string userId = User.GetSpecificClaim("Id");

                var user = _userManager.FindByIdAsync(userId).Result;
                if (userViewModel.FullName != null)
                {
                    user.FullName = userViewModel.FullName;
                }

                if (userViewModel.PhoneNumber != null)
                {
                    user.PhoneNumber = userViewModel.PhoneNumber;
                }

                _userManager.UpdateAsync(user);
                _unitOfWork.Commit();
                return new OkObjectResult(userViewModel);
            }
            else
            {
                return new BadRequestResult();
            }
        }

        #endregion AJAX API
    }
}