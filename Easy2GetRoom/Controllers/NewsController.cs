﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Models.NewsViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Easy2GetRoom.Controllers
{
    public class NewsController : Controller
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        [Route("news.html")]
        public IActionResult AllNews(string keyword, int pageSize = 9, int page = 1)
        {
            var news = new ListNewsViewModel();
            news.Data = _newsService.GetAllPaging(keyword, page, pageSize);

            return View(news);
        }


        [Route("easy2getroom-news-n.{id}.html", Name = "News detail")]
        public IActionResult Detail(int id)
        {
            var model = new DetailViewModel();
            model.News = _newsService.GetById(id);
            model.LastestNews = _newsService.GetLastestNews(3);
            return View(model);
        }
    }
}