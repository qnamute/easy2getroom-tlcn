﻿using Easy2GetRoom.Application.ViewModels;
using System;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IUserService : IDisposable
    {
        UserViewModel GetUser(string userId);

        void UpdatePassword();

        void UpdateAvatar();

        void UpdateInfo(UserViewModel userViewModel);
    }
}