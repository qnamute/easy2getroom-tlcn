﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.Interfaces.Client;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Extensions;
using Easy2GetRoom.Infrastructure.Interfaces;
using Easy2GetRoom.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Easy2GetRoom.Controllers
{
    [Authorize]
    public class ManagePostController : Controller
    {
        private readonly IPropertyService _propertyService;
        private readonly IUserManagePostService _userManagePostService;
        private readonly IUnitOfWork _unitOfWork;

        public ManagePostController(IPropertyService propertyService, IUserManagePostService userManagePostService, IUnitOfWork unitOfWork)
        {
            _propertyService = propertyService;
            _userManagePostService = userManagePostService;
            _unitOfWork = unitOfWork;
        }


        #region AJAX API
        [Route("manage-post.html")]
        public IActionResult Index()
        {
            string userId = User.GetSpecificClaim("Id");
            ManagePostViewModel managePostViewModel = new ManagePostViewModel();

            managePostViewModel.ActivePost = _userManagePostService.GetAllActivePropery(userId);
            managePostViewModel.InActivePost = _userManagePostService.GetAllInActivePropery(userId);
            managePostViewModel.AwaitingApprovalPost = _userManagePostService.GetAllAwaitingApprovalPropery(userId);

            return View(managePostViewModel);
        }

        public IActionResult GetAllActivePropertyPaging()
        {
            string userId = User.GetSpecificClaim("Id");
            ManagePostViewModel managePostViewModel = new ManagePostViewModel();
            managePostViewModel.ActivePost = _userManagePostService.GetAllActivePropery(userId);
            return new OkObjectResult(managePostViewModel);
        }

        public IActionResult GetAllInActivePropertyPaging()
        {
            string userId = User.GetSpecificClaim("Id");
            ManagePostViewModel managePostViewModel = new ManagePostViewModel();
            managePostViewModel.InActivePost = _userManagePostService.GetAllInActivePropery(userId);
            return new OkObjectResult(managePostViewModel);
        }

        public IActionResult GetAllAwaitingApprovalPropertyPaging()
        {
            string userId = User.GetSpecificClaim("Id");
            ManagePostViewModel managePostViewModel = new ManagePostViewModel();
            managePostViewModel.AwaitingApprovalPost = _userManagePostService.GetAllAwaitingApprovalPropery(userId);
            return new OkObjectResult(managePostViewModel);
        }

        [HttpPost]
        public IActionResult Hide(int id)
        {
            _propertyService.UpdateStatus(id, Status.InActive);
            _propertyService.Save();
            return new OkResult();
        }

        public IActionResult Delete(int id)
        {
            _propertyService.Delete(id);
            _propertyService.Save();

            return new OkResult();
        }

        public IActionResult Show(int id)
        {
            _propertyService.UpdateStatus(id, Status.Active);
            _propertyService.Save();
            return new OkResult();
        }

        #endregion
    }
}