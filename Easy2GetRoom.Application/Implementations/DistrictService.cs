﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Easy2GetRoom.Application.Implementations
{
    public class DistrictService : IDistrictService
    {
        private IDistrictRepository _districtRepository;
        private IUnitOfWork _unitOfWork;

        public DistrictService(IDistrictRepository districtRepository, IUnitOfWork unitOfWork)
        {
            _districtRepository = districtRepository;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _districtRepository.Remove(id);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<DistrictViewModel> GetAll()
        {
            return _districtRepository.FindAll().ProjectTo<DistrictViewModel>().ToList();
        }

        public DistrictViewModel GetById(int id)
        {
            var query = _districtRepository.FindById(id);
            var data = Mapper.Map<District, DistrictViewModel>(query);
            return data;
        }

        public List<DistrictViewModel> GetDistrictbyCityId(int id)
        {
            var data = _districtRepository.FindAll(x => x.CityId == id).OrderBy(x => x.Name).ProjectTo<DistrictViewModel>().ToList();
            return data;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(DistrictViewModel districtViewModel)
        {
            _districtRepository.Update(Mapper.Map<DistrictViewModel, District>(districtViewModel));
        }

        public void UpdateStatus(int id, Status status)
        {
            throw new NotImplementedException();
        }
    }
}
