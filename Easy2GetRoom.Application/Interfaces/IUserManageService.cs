﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Utilities.Dtos;
using System;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IUserManageService : IDisposable
    {
        PagedResult<User> GetAllPaging(Guid? role, string keyWord, int page, int pageSize);

        User GetById(string id);

        void Update(User user);

        void Delete(string Id);

        void Add(User user);

        void UpdateRole(User user, string role);
    }
}