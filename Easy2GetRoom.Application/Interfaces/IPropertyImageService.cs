﻿using Easy2GetRoom.Application.ViewModels;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IPropertyImageService : IDisposable
    {
        List<PropertyImageViewModel> GetPropertyImageByPropertyId(int id);

        void Save(PropertyImageViewModel propertyImageViewModel);
    }
}