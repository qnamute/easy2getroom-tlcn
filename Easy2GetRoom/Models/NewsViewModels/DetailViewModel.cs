﻿using Easy2GetRoom.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy2GetRoom.Models.NewsViewModels
{
    public class DetailViewModel
    {
        public List<NewsViewModel> LastestNews { get; set; }

        public NewsViewModel News { get; set; }
    }
}
