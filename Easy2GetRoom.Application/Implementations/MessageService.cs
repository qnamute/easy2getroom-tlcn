﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using Easy2GetRoom.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Application.Implementations
{
    public class MessageService : IMessageService
    {
        private IMessageRepository _messageRepository;
        private IUnitOfWork _unitOfWork;

        public MessageService(IMessageRepository messageRepository, IUnitOfWork unitOfWork)
        {
            _messageRepository = messageRepository;
            _unitOfWork = unitOfWork;
        }

        public MessageViewModel Add(MessageViewModel messageViewModel)
        {
            var message = Mapper.Map<MessageViewModel, Message>(messageViewModel);
            _messageRepository.Add(message);
            _unitOfWork.Commit();
            return Mapper.Map<Message, MessageViewModel>(message);
        }

        public void Delete(int id)
        {
            _messageRepository.Remove(id);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<MessageViewModel> GetAll(ReplyMessage? isReply)
        {
            return _messageRepository.FindAll().ProjectTo<MessageViewModel>().ToList();
        }

        public PagedResult<MessageViewModel> GetAllPaging(ReplyMessage? isReply, int page, int pageSize)
        {
            var query = _messageRepository.FindAll(x => x.UserSender, x => x.ParentId);
            query = query.Where(x => x.ParentId == 0);
            if (isReply != null)
            {
                query = query.Where(x => x.IsReply == isReply);
            }
            // Paging
            int totalRow = query.Count();

            query = query.Skip((page - 1) * pageSize).Take(pageSize).OrderByDescending(x => x.DateCreated);

            var data = query.ProjectTo<MessageViewModel>().ToList();

            var paginationSet = new PagedResult<MessageViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public MessageViewModel GetById(int id)
        {
            return Mapper.Map<Message, MessageViewModel>(_messageRepository.FindById(id));
        }

        public void UpdateStatus(int id, ReplyMessage isReply)
        {
            var message = _messageRepository.FindById(id);
            message.IsReply = isReply;
            _messageRepository.Update(message);
        }
    }
}