﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Easy2GetRoom.Application.Implementations
{
    public class RentalTypeService : IRentalTypeService
    {
        private IRentalTypeRepository _rentalTypeRepository;
        private IUnitOfWork _unitOfWork;

        public RentalTypeService(IRentalTypeRepository rentalTypeRepository, IUnitOfWork unitOfWork)
        {
            _rentalTypeRepository = rentalTypeRepository;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _rentalTypeRepository.Remove(id);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<RentalTypeViewModel> GetAll()
        {
            return _rentalTypeRepository.FindAll().ProjectTo<RentalTypeViewModel>().ToList();
        }

        public RentalTypeViewModel GetById(int id)
        {
            var query = _rentalTypeRepository.FindById(id);
            var data = Mapper.Map<RentalType, RentalTypeViewModel>(query);
            return data;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(RentalTypeViewModel rentalTypeViewModel)
        {
            _rentalTypeRepository.Update(Mapper.Map<RentalTypeViewModel, RentalType>(rentalTypeViewModel));
        }

        public void UpdateStatus(int id, Status status)
        {
            throw new NotImplementedException();
        }
    }
}
