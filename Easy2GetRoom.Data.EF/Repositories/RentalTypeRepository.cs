﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Data.EF.Repositories
{
    public class RentalTypeRepository : EFRepository<RentalType, int>, IRentalTypeRepository
    {
        public RentalTypeRepository(AppDbContext context) : base(context)
        {
        }
    }
}
