﻿using Easy2GetRoom.Application.ViewModels;
using System.Collections.Generic;

namespace Easy2GetRoom.Models
{
    public class ManagePostViewModel
    {
        public List<PropertyViewModel> ActivePost { get; set; }

        public List<PropertyViewModel> InActivePost { get; set; }

        public List<PropertyViewModel> AwaitingApprovalPost { get; set; }
    }
}