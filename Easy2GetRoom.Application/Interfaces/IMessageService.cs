﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Utilities.Dtos;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IMessageService : IDisposable
    {
        MessageViewModel Add(MessageViewModel messageViewModel);

        List<MessageViewModel> GetAll(ReplyMessage? isReply);

        PagedResult<MessageViewModel> GetAllPaging(ReplyMessage? isReply, int page, int pageSize);

        MessageViewModel GetById(int id);

        void UpdateStatus(int id, ReplyMessage isReply);

        void Delete(int id);
    }
}