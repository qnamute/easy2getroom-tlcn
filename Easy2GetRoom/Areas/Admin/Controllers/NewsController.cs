﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class NewsController : BaseController
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region AJAX API

        [HttpPost]
        public IActionResult SaveEntity(NewsViewModel newsViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (User.Identity.IsAuthenticated)
                {
                    Guid userId = Guid.Parse(User.GetSpecificClaim("Id"));
                    newsViewModel.UserId = userId;
                    if (newsViewModel.Id == 0)
                    {
                        newsViewModel = _newsService.Add(newsViewModel);
                    }
                    else
                    {
                        _newsService.Update(newsViewModel);
                    }
                    return new OkObjectResult(newsViewModel);
                }
                else return new BadRequestResult();
            }
        }

        public IActionResult GetAllPaging(string keyWord, int page, int pageSize)
        {
            var model = _newsService.GetAllPaging(keyWord, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            var model = _newsService.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            _newsService.Delete(id);
            return new OkResult();
        }

        #endregion AJAX API
    }
}