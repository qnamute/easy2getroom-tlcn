﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Data.Enums
{
    public enum ReplyMessage
    {
        NotReplied,
        Replied
    }
}
