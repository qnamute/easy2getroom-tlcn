﻿var contactController = function () {
    this.initialize = function () {
        registerEvent();
    }

    function registerEvent() {
        $('#btnSend').on('click', function () {
            saveMessage();
        });

        $('#notifyModal').on('hidden.bs.modal', function () {
            console.log('Sdasd');
            $(location).attr('href', 'https://localhost:44389');
        });
    }

    function saveMessage() {
        let email = $('#txtEmail').val();
        let title = $('#txtTitle').val();
        let content = $('#txtContent').val();

        $.ajax({
            type: 'post',
            url: '/contact/SaveEntity',
            data: {
                EmailSender: email,
                Title: title,
                Content: content
            },
            dataType: 'json',
            success: function (response) {
                $('#notifyModal').modal('show');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
}