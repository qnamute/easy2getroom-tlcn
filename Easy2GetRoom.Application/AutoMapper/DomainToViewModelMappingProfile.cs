﻿using AutoMapper;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;

namespace Easy2GetRoom.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<PropertyCategory, PropertyCategoryViewModel>();
            CreateMap<Function, FunctionViewModel>();
            CreateMap<Property, PropertyViewModel>();
            CreateMap<User, UserViewModel>();
            CreateMap<RentalType, RentalTypeViewModel>();
            CreateMap<City, CityViewModel>();
            CreateMap<District, DistrictViewModel>();
            CreateMap<Wards, WardsViewModel>();
            CreateMap<News, NewsViewModel>();
            CreateMap<NewsImage, NewsImageViewModel>();
            
        }
    }
}