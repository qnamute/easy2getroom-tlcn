﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.ViewModels
{
    public class PropertyImageViewModel
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public int PropertyId { get; set; }

        public PropertyViewModel Property { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }


    }
}
