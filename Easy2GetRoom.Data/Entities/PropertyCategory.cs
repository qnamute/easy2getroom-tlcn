﻿using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Easy2GetRoom.Data.Entities
{
    [Table("PropertyCategories")]
    public class PropertyCategory : DomainEntity<int>, ISwitchable, ISortable, IDateTracking
    {
        public PropertyCategory()
        {
            Properties = new HashSet<Property>();
        }

        public PropertyCategory(string name, string description, Status status, int sortOrder)
        {
            Name = name;
            Description = description;
            Status = status;
            SortOrder = sortOrder;
        }

        public PropertyCategory(int id, string name, string description, Status status, int sortOrder)
        {
            Id = id;
            Name = name;
            Description = description;
            Status = status;
            SortOrder = sortOrder;
        }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [StringLength(1000)]
        [Required]
        public string Description { get; set; }

        public Status Status { get; set; }

        public int SortOrder { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public virtual ICollection<Property> Properties { get; set; }
    }
}