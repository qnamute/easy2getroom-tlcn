﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.ViewModels
{
    public class NewsImageViewModel
    {
        public int Id { get; set; }
        public string Url { get; set; }

        public int NewsId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public NewsViewModel News { get; set; }
    }
}
