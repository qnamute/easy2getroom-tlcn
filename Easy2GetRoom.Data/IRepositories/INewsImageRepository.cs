﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Infrastructure.Interfaces;

namespace Easy2GetRoom.Data.IRepositories
{
    public interface INewsImageRepository : IRepository<NewsImage, int>
    {
    }
}