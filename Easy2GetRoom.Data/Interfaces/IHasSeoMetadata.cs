﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Data.Interfaces
{
    public interface IHasSeoMetadata
    {
        string SeoPageTitle { get; set; }
        string SeoAlias { get; set; }
        string SeoKeywords { get; set; }
        string SeoDescriptions { get; set; }
    }
}
