TRƯỜNG ĐẠI HỌC SƯ PHẠM KỸ THUẬT THÀNH PHỐ HỒ CHÍ MINH
KHOA CÔNG NGHỆ THÔNG TIN 
PROJECT TIỂU LUẬN CHUYÊN NGÀNH - CÔNG NGHỆ PHẦN MỀM 


Đề tài: Xây dựng website tìm và cho thuê phòng trọ sử dụng ASP.Net Core và Jquery

GVHD: Thầy Mai Tuấn Khôi
GVPB: Thầy Nguyễn Minh Đạo

Sinh viên thực hiện: 	
Đinh Quang Nam 	     	- 16110392
Nguyễn Thị Thanh Thủy 	- 16110478

			CÁC BƯỚC CÀI ĐẶT PHẦN MỀM: EASY2GETROOM
1. Cài đặt Database MySQL (phiên bản 8.0.18) trên Docker (hoặc cài mysql trên trang chủ của mysql)
2. Cài đặt Visual Studio => Asp.Net Core (Version 2.2)
3. Cài đặt Các Extension
4. Điều chỉnh chuỗi kết nối với Database
	appsetting.json => defaultConnection
	User mặc định: root
	Password mặc định: 1234
5. Update Database
	1. Tools => Nuget Package Manager => Package Manager Console
	2. Chọn Default Project: 3-Domain\Easy2GetRoom.Data.EF
	3. Gõ lệnh "update-database", Database sẽ tự động được thêm vào CSDL
6.  Chạy Project
- Hoàn tất việc thiết lập và chạy project. Nhóm em xin cảm ơn !
