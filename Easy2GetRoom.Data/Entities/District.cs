﻿using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("Districts")]
    public class District: DomainEntity<int>
    {
        public District()
        {
            Wards = new HashSet<Wards>();
            Properties = new HashSet<Property>();
        }

        public District(int cityId, string name, City city, ICollection<Wards> wards, ICollection<Property> properties)
        {
            CityId = cityId;
            Name = name;
            City = city;
            Wards = wards;
            Properties = properties;
        }

        public District(int id, int cityId, string name, City city, ICollection<Wards> wards, ICollection<Property> properties)
        {
            Id = id;
            CityId = cityId;
            Name = name;
            City = city;
            Wards = wards;
            Properties = properties;
        }

        public int CityId { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        public virtual ICollection<Wards> Wards { get; set; }

        public virtual ICollection<Property> Properties { get; set; }
    }
}
