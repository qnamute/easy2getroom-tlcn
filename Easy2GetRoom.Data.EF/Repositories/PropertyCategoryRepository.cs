﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Data.EF.Repositories
{
    public class PropertyCategoryRepository : EFRepository<PropertyCategory, int>, IPropertyCategoryRepository
    {
        AppDbContext _context;
        public PropertyCategoryRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        //Interface implement 

        //When add ToList(),... method, the query script is excuted
    }
}
