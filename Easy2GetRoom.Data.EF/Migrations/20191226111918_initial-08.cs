﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Easy2GetRoom.Data.EF.Migrations
{
    public partial class initial08 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "HasSeen",
                table: "Messages",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasSeen",
                table: "Messages");
        }
    }
}
