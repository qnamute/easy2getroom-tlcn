﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Easy2GetRoom.Application.Implementations
{
    public class CityService : ICityService
    {
        private ICityRepository _cityRepository;
        private IUnitOfWork _unitOfWork;

        public CityService(ICityRepository cityRepository, IUnitOfWork unitOfWork)
        {
            _cityRepository = cityRepository;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _cityRepository.Remove(id);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<CityViewModel> GetAll()
        {
            return _cityRepository.FindAll().OrderBy(x=>x.Name).ProjectTo<CityViewModel>().ToList();
        }

        public CityViewModel GetById(int id)
        {
            return Mapper.Map<City, CityViewModel>(_cityRepository.FindById(id));
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(CityViewModel cityViewModel)
        {
            _cityRepository.Update(Mapper.Map<CityViewModel, City>(cityViewModel));
        }

        public void UpdateStatus(int id, Status status)
        {
            throw new NotImplementedException();
        }
    }
}
