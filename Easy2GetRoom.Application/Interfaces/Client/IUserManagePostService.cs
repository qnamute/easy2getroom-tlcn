﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.Interfaces.Client
{
    public interface IUserManagePostService : IDisposable
    {
        List<PropertyViewModel> GetAllActivePropery(string userId);

        List<PropertyViewModel> GetAllInActivePropery(string userId);

        List<PropertyViewModel> GetAllAwaitingApprovalPropery(string userId);

        void UpdateStatus(PropertyViewModel propertyViewModel);

        void Delete(int id);
    }
}