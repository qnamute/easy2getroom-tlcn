﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Utilities.Dtos;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy2GetRoom.Models.PropertyViewModels
{
    public class CatalogViewModel
    {
        public PagedResult<PropertyViewModel> Data { get; set; }

        public PropertyCategoryViewModel Category { get; set; }

        public string SortType { get; set; }

        public int? PageSize { get; set; }

        public List<SelectListItem> SortTypes { get; } = new List<SelectListItem>
        {
            new SelectListItem() {Value = "lastest", Text = "Mới nhất"},
            new SelectListItem() {Value = "price", Text = "Giá"},
            new SelectListItem() {Value = "name", Text= "Tên"},
        };
    }
}
