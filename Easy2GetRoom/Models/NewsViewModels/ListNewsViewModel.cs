﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Utilities.Dtos;

namespace Easy2GetRoom.Models.NewsViewModels
{
    public class ListNewsViewModel
    {
        public PagedResult<NewsViewModel> Data { get; set; }

    }
}