﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;

namespace Easy2GetRoom.Data.EF.Repositories
{
    public class MessageImageRepository : EFRepository<MessageImage, int>, IMessageImageRepository
    {
        public MessageImageRepository(AppDbContext context) : base(context)
        {
        }
    }
}