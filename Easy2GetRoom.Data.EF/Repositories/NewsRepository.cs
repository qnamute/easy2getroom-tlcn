﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;

namespace Easy2GetRoom.Data.EF.Repositories
{
    public class NewsRepository : EFRepository<News, int>, INewsRepository
    {
        public NewsRepository(AppDbContext context) : base(context)
        {
        }
    }
}