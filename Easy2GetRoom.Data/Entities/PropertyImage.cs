﻿using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("PropertyImages")]
    public class PropertyImage : DomainEntity<int>, IDateTracking
    {
        public PropertyImage()
        {
        }

        public PropertyImage(string url, int propertyId)
        {
            Url = url;
            PropertyId = propertyId;
        }

        public PropertyImage(int id, string url, int propertyId)
        {
            Id = id;
            Url = url;
            PropertyId = propertyId;
        }

        public PropertyImage(string url, int propertyId, DateTime dateCreated, DateTime dateModified)
        {
            Url = url;
            PropertyId = propertyId;
            DateCreated = dateCreated;
            DateModified = dateModified;
        }

        [StringLength(1000)]
        [Required]
        public string Url { get; set; }

        public int PropertyId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public virtual Property Property { get; set; }
    }
}
