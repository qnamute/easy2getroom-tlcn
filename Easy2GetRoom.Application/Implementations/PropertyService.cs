﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using Easy2GetRoom.Utilities.Constants;
using Easy2GetRoom.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Application.Implementations
{
    public class PropertyService : IPropertyService
    {
        private IPropertyRepository _propertyRepository;
        private IPropertyImageRepository _propertyImageRepository;
        private IUnitOfWork _unitOfWork;

        public PropertyService(IPropertyRepository propertyRepository, IPropertyImageRepository propertyImageRepository,
                IUnitOfWork unitOfWork)
        {
            _propertyRepository = propertyRepository;
            _propertyImageRepository = propertyImageRepository;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _propertyRepository.Remove(id);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<PropertyViewModel> GetAll()
        {
            return _propertyRepository.FindAll(x => x.PropertyCategory, y => y.RentalType).ProjectTo<PropertyViewModel>().ToList();
        }

        public PagedResult<PropertyViewModel> GetAllPaging(int? categoryId, string keyWord, int page, int pageSize)
        {
            var query = _propertyRepository.FindAll();
            if (!string.IsNullOrEmpty(keyWord))
            {
                query = query.Where(x => x.Name.Contains(keyWord) || x.Title.Contains(keyWord) || x.City.Name.Contains(keyWord)
                    || x.District.Name.Contains(keyWord) || x.Wards.Name.Contains(keyWord) || x.RentalType.Name.Contains(keyWord));
            }
            if (categoryId.HasValue)
            {
                query = query.Where(x => x.PropertyCategoryId == categoryId);
            }

            int totalRow = query.Count();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            var data = query.ProjectTo<PropertyViewModel>().ToList();

            var paginationSet = new PagedResult<PropertyViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public PropertyViewModel GetById(int id)
        {
            var query = _propertyRepository.FindById(id, p => p.City, p => p.District, p => p.Wards, p => p.RentalType, p => p.User, p => p.PropertyCategory);
            var data = Mapper.Map<Property, PropertyViewModel>(query);
            return data;
        }

        public void Update(PropertyViewModel propertyViewModel)
        {
            _propertyRepository.Update(Mapper.Map<PropertyViewModel, Property>(propertyViewModel));
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public PagedResult<PropertyViewModel> GetAllActivePropertyPaging(int? categoryId, string keyWord, int page, int pageSize, string userId)
        {
            var query = _propertyRepository.FindAll(x => x.Status == Status.Active);
            //Checkuser id

            if (!string.IsNullOrEmpty(userId))
            {
                query = query.Where(x => x.UserId.Equals(userId));
            }

            if (!string.IsNullOrEmpty(keyWord))
            {
                query = query.Where(x => x.Name.Contains(keyWord) || x.Title.Contains(keyWord) || x.City.Name.Contains(keyWord)
                || x.District.Name.Contains(keyWord));
            }
            if (categoryId.HasValue)
            {
                query = query.Where(x => x.PropertyCategoryId == categoryId);
            }
            //Get Active post
            query = query.Where(x => x.Status == Status.Active);

            int totalRow = query.Count();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            var data = query.ProjectTo<PropertyViewModel>().ToList();

            var paginationSet = new PagedResult<PropertyViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public PagedResult<PropertyViewModel> GetAllInActivePropertyPaging(int? categoryId, string keyWord, int page, int pageSize, string userId)
        {
            //var query = _propertyRepository.FindAll(x => x.Status == Status.Active);
            var query = _propertyRepository.FindAll();
            //Checkuser id

            if (!string.IsNullOrEmpty(userId))
            {
                query = query.Where(x => x.UserId.Equals(userId));
            }

            if (!string.IsNullOrEmpty(keyWord))
            {
                query = query.Where(x => x.Name.Contains(keyWord));
            }
            if (categoryId.HasValue)
            {
                query = query.Where(x => x.PropertyCategoryId == categoryId);
            }
            //Get Active post
            query = query.Where(x => x.Status == Status.InActive);

            int totalRow = query.Count();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            var data = query.ProjectTo<PropertyViewModel>().ToList();

            var paginationSet = new PagedResult<PropertyViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public PagedResult<PropertyViewModel> GetAllAwaitingApprovalPropertyPaging(int? categoryId, string keyWord, int page, int pageSize, string userId)
        {
            //var query = _propertyRepository.FindAll(x => x.Status == Status.Active);
            var query = _propertyRepository.FindAll();
            //Checkuser id

            if (!string.IsNullOrEmpty(userId))
            {
                query = query.Where(x => x.UserId.Equals(userId));
            }

            if (!string.IsNullOrEmpty(keyWord))
            {
                query = query.Where(x => x.Name.Contains(keyWord));
            }
            if (categoryId.HasValue)
            {
                query = query.Where(x => x.PropertyCategoryId == categoryId);
            }
            //Get awaiting approval post
            query = query.Where(x => x.Status == Status.AwaitingApproval);

            int totalRow = query.Count();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            var data = query.ProjectTo<PropertyViewModel>().ToList();

            var paginationSet = new PagedResult<PropertyViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public void UpdateStatus(int id, Status status)
        {
            var temp = _propertyRepository.FindById(id);
            if (temp != null)
            {
                temp.Status = status;
            }

            _propertyRepository.Update(temp);
        }

        public void AddImages(string[] imagesUrls, int propertyId)
        {
            _propertyImageRepository.RemoveMultiple(_propertyImageRepository.FindAll(x => x.PropertyId == propertyId).ToList());
            foreach (var url in imagesUrls)
            {
                _propertyImageRepository.Add(new PropertyImage()
                {
                    Url = url,
                    PropertyId = propertyId
                });
            }
        }

        public PropertyViewModel Add(PropertyViewModel propertyViewModel)
        {
            var property = Mapper.Map<PropertyViewModel, Property>(propertyViewModel);

            _propertyRepository.Add(property);
            _unitOfWork.Commit();
            propertyViewModel = Mapper.Map<Property, PropertyViewModel>(property);
            return propertyViewModel;
        }

        public UserViewModel GetPropertyOwnerByPropertyId(int id)
        {
            throw new NotImplementedException();
        }

        public List<PropertyViewModel> GetLastestProperties()
        {
            var query = _propertyRepository
                .FindAll()
                .OrderByDescending(p => p.DateCreated)
                .Where(x => x.Status == Status.Active)
                .Take(CommonConstants.AmountLastestProperty)
                .ProjectTo<PropertyViewModel>().ToList();
            return query;
        }

        public PagedResult<PropertyViewModel> GetFilterPaging(int? categoryId, int? rentalTypeId, int? cityId, int? districtId,
            int? wardsId, int? acreageRange, int? priceRange, int page, int pageSize)
        {
            var query = _propertyRepository.FindAll();
            if (categoryId != null)
            {
                query = query.Where(x => x.PropertyCategoryId == categoryId);
            }
            if (rentalTypeId != null)
            {
                query = query.Where(x => x.RentalTypeId == rentalTypeId);
            }
            if (cityId != null)
            {
                query = query.Where(x => x.CityId == cityId);
            }
            if (districtId != null)
            {
                query = query.Where(x => x.DistrictId == districtId);
            }
            if (wardsId != null)
            {
                query = query.Where(x => x.WardsId == wardsId);
            }
            if (acreageRange != null)
            {
                switch (acreageRange)
                {
                    case 1:
                        // Acreage from 0 => 30
                        query = query.Where(x => x.Acreage >= 0 || x.Acreage <= 30);
                        break;
                    case 2:
                        // Acreage from 30 => 80 
                        query = query.Where(x => x.Acreage >= 30 || x.Acreage <= 80);
                        break;
                    case 3:
                        // Acreage from 80 => 150 
                        query = query.Where(x => x.Acreage >= 80 || x.Acreage <= 150);
                        break;
                    case 4:
                        // Acreage from 150 =>300 
                        query = query.Where(x => x.Acreage >= 150 || x.Acreage <= 300);
                        break;
                    case 5:
                        // Acreage from lager than 300 
                        query = query.Where(x => x.Acreage > 300);
                        break;
                }
            }
            if (priceRange != null)
            {
                switch (priceRange)
                {
                    case 1:
                        // Price from 0 => 3 
                        query = query.Where(x => x.Price >= 0 || x.Price <= 3000000);
                        break;
                    case 2:
                        // Price from 3 => 5 
                        query = query.Where(x => x.Price >= 3000000 || x.Price <= 5000000);
                        break;
                    case 3:
                        // Price from 5 => 10 
                        query = query.Where(x => x.Price >= 5000000 || x.Price <= 10000000);
                        break;
                    case 4:
                        // Price from 10 => 20 
                        query = query.Where(x => x.Price >= 10000000 || x.Price <= 20000000);
                        break;
                    case 5:
                        // Price from lager than 20 
                        query = query.Where(x => x.Price >= 20000000);
                        break;
                }
            }

            //Get Active post
            query = query.Where(x => x.Status == Status.Active);

            int totalRow = query.Count();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            var data = query.ProjectTo<PropertyViewModel>().ToList();

            var paginationSet = new PagedResult<PropertyViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public List<PropertyViewModel> GetRelatedProperties(int id, int top)
        {
            var property = _propertyRepository.FindById(id);
            return _propertyRepository.FindAll(x => x.Status == Status.Active
                && x.Id != property.Id && x.PropertyCategoryId == property.PropertyCategoryId)
                .Take(top)
                .ProjectTo<PropertyViewModel>()
                .ToList();
        }

        public List<PropertyViewModel> GetAllSlideProperty()
        {
            var properties = new List<PropertyViewModel>();
            properties = _propertyRepository.FindAll(x => x.SlideFlag == 1).ProjectTo<PropertyViewModel>().ToList();
            return properties;
        }

        public void UpdateSlideFlag(int id, int isFlag)
        {
            var temp = _propertyRepository.FindById(id);
            if (temp != null)
            {
                temp.SlideFlag = isFlag;
            }

            _propertyRepository.Update(temp);
        }
    }
}