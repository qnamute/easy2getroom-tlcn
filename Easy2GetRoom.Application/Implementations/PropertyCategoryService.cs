﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Easy2GetRoom.Application.Implementations
{
    public class PropertyCategoryService : IPropertyCategoryService
    {

        private IPropertyCategoryRepository _propertyCategoryRepository;
        private IUnitOfWork _unitOfWork;

        public PropertyCategoryService(IPropertyCategoryRepository propertyCategoryRepository, IUnitOfWork unitOfWork)
        {
            _propertyCategoryRepository = propertyCategoryRepository;
            _unitOfWork = unitOfWork;
        }

        public PropertyCategoryViewModel Add(PropertyCategoryViewModel propertyCategoryViewModal)
        {
            //_propertyCategoryRepository.Add(propertyCategoryViewModal);
            var productCategory = Mapper.Map<PropertyCategoryViewModel, PropertyCategory>(propertyCategoryViewModal);
            _propertyCategoryRepository.Add(productCategory);

            return propertyCategoryViewModal;
        }

        public void Delete(int id)
        {
            _propertyCategoryRepository.Remove(id);
        }

        public List<PropertyCategoryViewModel> GetAll()
        {
            return _propertyCategoryRepository.FindAll()
                .ProjectTo<PropertyCategoryViewModel>().ToList();
        }

        public List<PropertyCategoryViewModel> GetAll(string keyword)
        {
            if (!string.IsNullOrEmpty(keyword))
            {
                return _propertyCategoryRepository.FindAll(x => x.Name.Contains(keyword) ||
                    x.Description.Contains(keyword))
                    .ProjectTo<PropertyCategoryViewModel>().ToList();
            }
            else
            {
                return _propertyCategoryRepository.FindAll()
                .ProjectTo<PropertyCategoryViewModel>().ToList();
            }
        }

        public PropertyCategoryViewModel GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(PropertyCategoryViewModel propertyCategoryViewModal)
        {
            throw new NotImplementedException();
        }
    }
}
