﻿using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.ViewModels
{
    public class PropertyCategoryViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Status Status { get; set; }

        public int SortOrder { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }
    }
}