﻿using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Easy2GetRoom.Data.Entities
{
    [Table("Properties")]
    public class Property : DomainEntity<int>, ISwitchable, IDateTracking
    {
        public Property()
        {
        }

        public Property(int id, string name, int propertyCategoryId, int cityId, int districtId, int wardsId, int rentalTypeId, string title, string description, decimal price, decimal priceFrom, decimal priceTo, float acreage, float acreageFrom, float acreageTo, string address, double? lat, double? lng, Status status, int slideFlag, Guid? userId)
        {
            Id = id;
            Name = name;
            PropertyCategoryId = propertyCategoryId;
            CityId = cityId;
            DistrictId = districtId;
            WardsId = wardsId;
            RentalTypeId = rentalTypeId;
            Title = title;
            Description = description;
            Price = price;
            PriceFrom = priceFrom;
            PriceTo = priceTo;
            Acreage = acreage;
            AcreageFrom = acreageFrom;
            AcreageTo = acreageTo;
            Address = address;
            Lat = lat;
            Lng = lng;
            Status = status;
            SlideFlag = slideFlag;
            UserId = userId;
        }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        [Required]
        public int PropertyCategoryId { get; set; }

        [Required]
        public int CityId { get; set; }

        [Required]
        public int DistrictId { get; set; }

        [Required]
        public int WardsId { get; set; }

        [Required]
        public int RentalTypeId { get; set; }

        [StringLength(255)]
        [Required]
        public string Title { get; set; }

        [StringLength(1000)]
        [Required]
        public string Description { get; set; }

        public decimal Price { get; set; }

        public decimal PriceFrom { get; set; }

        public decimal PriceTo { get; set; }

        public float Acreage { get; set; }

        public float AcreageFrom { get; set; }

        public float AcreageTo { get; set; }

        public string Address { get; set; }

        public double? Lat { get; set; }

        public double? Lng { get; set; }

        public Status Status { get; set; }

        public int SlideFlag { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public Guid? UserId { get; set; }

        [ForeignKey("PropertyCategoryId")]
        public virtual PropertyCategory PropertyCategory { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        [ForeignKey("DistrictId")]
        public virtual District District { get; set; }

        [ForeignKey("WardsId")]
        public virtual Wards Wards { get; set; }

        [ForeignKey("RentalTypeId")]
        public virtual RentalType RentalType { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public virtual ICollection<PropertyImage> PropertyImages { get; set; }
    }
}