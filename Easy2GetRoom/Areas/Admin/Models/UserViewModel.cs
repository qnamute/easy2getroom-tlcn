﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Utilities.Dtos;
using System.Collections.Generic;

namespace Easy2GetRoom.Areas.Admin.Models
{
    public class UserViewModel
    {
        public PagedResult<User> Users { get; set; }

        public List<string> Roles { get; set; }

        public User User { get; set; }

        public string Role { get; set; }
    }
}