﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface ICityService : IDisposable
    {
        List<CityViewModel> GetAll();

        CityViewModel GetById(int id);

        void UpdateStatus(int id, Status status);

        void Delete(int id);

        void Update(CityViewModel cityViewModel);

        void Save();
    }
}