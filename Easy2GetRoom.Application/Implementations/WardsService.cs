﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Easy2GetRoom.Application.Implementations
{
    public class WardsService : IWardsService
    {
        private IWardsRepository _wardsRepository;
        private IUnitOfWork _unitOfWork;

        public WardsService(IWardsRepository wardsRepository, IUnitOfWork unitOfWork)
        {
            _wardsRepository = wardsRepository;
            _unitOfWork = unitOfWork;
        }

        public void Delete(int id)
        {
            _wardsRepository.Remove(id);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<WardsViewModel> GetAll()
        {
            return _wardsRepository.FindAll().ProjectTo<WardsViewModel>().ToList();
        }

        public WardsViewModel GetById(int id)
        {
            var query = _wardsRepository.FindById(id);
            var data = Mapper.Map<Wards, WardsViewModel>(query);
            return data;
        }

        public List<WardsViewModel> GetWardsByDistrictId(int id)
        {
            var data = _wardsRepository.FindAll(x => x.DistrictId == id).OrderBy(x => x.Name).ProjectTo<WardsViewModel>().ToList();
            return data;
        }

        public void Save()
        {
            _unitOfWork.Commit();
        }

        public void Update(WardsViewModel wardsViewModel)
        {
            _wardsRepository.Update(Mapper.Map<WardsViewModel, Wards>(wardsViewModel));
        }

        public void UpdateStatus(int id, Status status)
        {
            throw new NotImplementedException();
        }
    }
}
