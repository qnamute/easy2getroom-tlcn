﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Easy2GetRoom.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class DistrictController : BaseController
    {
        private IDistrictService _districService;
        private ICityService _cityService;
        public DistrictController(IDistrictService districSerbice, ICityService cityService)
        {
            _districService = districSerbice;
            _cityService = cityService;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetgetDistrictbyCityId(int id)
        {
            var data = _districService.GetDistrictbyCityId(id);
            return new OkObjectResult(data);
        }
        

        public IActionResult GetAll()
        {
            var model = _cityService.GetAll();
            return new OkObjectResult(model);
        }
    }
}