﻿using AutoMapper;
using Easy2GetRoom.Application.Implementations;
using Easy2GetRoom.Application.Implementations.Client;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.Interfaces.Client;
using Easy2GetRoom.Data.EF;
using Easy2GetRoom.Data.EF.Repositories;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Helpers;
using Easy2GetRoom.Infrastructure.Interfaces;
using Easy2GetRoom.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace Easy2GetRoom
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // add migrations assembly khi chạy
            // lấy kết nối tới csdl the đường dẩn trong appsettings.json

            var connection =
            services.AddDbContext<AppDbContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection"), mysqlOptions =>
                {
                    mysqlOptions
                        .ServerVersion(new Version(8, 0, 18), ServerType.MySql)
                        .CharSetBehavior(CharSetBehavior.AppendToAllColumns)
                        .AnsiCharSet(CharSet.Latin1)
                        .UnicodeCharSet(CharSet.Utf8mb4);
                }));
            services.AddIdentity<User, Role>()
                .AddRoles<Role>()
                .AddRoleManager<RoleManager<Role>>()
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<AppDbContext>();

            //Config identity
            services.Configure<IdentityOptions>(options =>
            {
                //Password setting
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                //Lockout setting
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 10;

                //User setting
                options.User.RequireUniqueEmail = true;
            });

            //automapper
            services.AddAutoMapper();

            //Add Applicatiton Services
            services.AddScoped<UserManager<User>, UserManager<User>>();
            services.AddScoped<RoleManager<Role>, RoleManager<Role>>();
            services.AddScoped<IUserClaimsPrincipalFactory<User>, CustomClaimsPrincipalFactory>();

            //Mapping domain and viewmodal
            services.AddSingleton(Mapper.Configuration);
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<AutoMapper.IConfigurationProvider>(), sp.GetService));

            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IViewRenderService, ViewRenderService>();

            services.AddTransient<DbInitializer>();

            //Add user claim sercice
            services.AddScoped<IUserClaimsPrincipalFactory<User>, CustomClaimsPrincipalFactory>();

            //add
            services.AddMvc(options =>
            {
                options.CacheProfiles.Add("Default",
                    new CacheProfile()
                    {
                        Duration = 60
                    });
                options.CacheProfiles.Add("Never",
                    new CacheProfile()
                    {
                        Location = ResponseCacheLocation.None,
                        NoStore = true
                    });
            }).AddViewLocalization(
                   LanguageViewLocationExpanderFormat.Suffix,
                   opts => { opts.ResourcesPath = "Resources"; })
               .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
               .AddDataAnnotationsLocalization()
               .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    IConfigurationSection googleAuthNSection =
                        Configuration.GetSection("Authentication:Google");
                    options.ClientId = "917101834646-b9akd2m7tg3goaqmtiq0rstbkggprb9t.apps.googleusercontent.com";
                    options.ClientSecret = "KEqhCAmwJkDeLZBXhuviaxI-";
                })
                .AddFacebook(options =>
                {
                    options.AppId = "544360443014437";
                    options.AppSecret = "671614221328ba26634695d0df31a124";
                });

            services.AddTransient(typeof(IUnitOfWork), typeof(EFUnitOfWork));
            services.AddTransient(typeof(IRepository<,>), typeof(EFRepository<,>));

            services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });

            services.Configure<RequestLocalizationOptions>(
              opts =>
              {
                  var supportedCultures = new List<CultureInfo>
                  {
                        new CultureInfo("en-US"),
                        new CultureInfo("vi-VN")
                  };

                  opts.DefaultRequestCulture = new RequestCulture("en-US");
                  // Formatting numbers, dates, etc.
                  opts.SupportedCultures = supportedCultures;
                  // UI strings that we have localized.
                  opts.SupportedUICultures = supportedCultures;
              });

            // ===== Configure Identity =======
            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.Name = "auth_cookie";
                options.Cookie.SameSite = SameSiteMode.None;
                options.LoginPath = new PathString("/login.html");
                options.AccessDeniedPath = new PathString("/login.html");
            });


            //Declare Repository
            services.AddTransient<IPropertyCategoryRepository, PropertyCategoryRepository>();
            services.AddTransient<IFunctionRepository, FunctionRepository>();
            services.AddTransient<IPropertyRepository, PropertyRepository>();
            services.AddTransient<IRentalTypeRepository, RentalTypeRepository>();
            services.AddTransient<ICityRepository, CityRepository>();
            services.AddTransient<IDistrictRepository, DistrictRepository>();
            services.AddTransient<IWardsRepository, WardsRepository>();
            services.AddTransient<IPropertyImageRepository, PropertyImageRepository>();
            services.AddTransient<INewsRepository, NewsRepository>();
            services.AddTransient<IMessageRepository, MessageRepository>();
            services.AddTransient<IMessageImageRepository, MessageImageRepository>();

            //Delcare Service
            services.AddTransient<IPropertyCategoryService, PropertyCategoryService>();
            services.AddTransient<IFunctionService, FunctionService>();
            services.AddTransient<IPropertyService, PropertyService>();
            services.AddTransient<IRentalTypeService, RentalTypeService>();
            services.AddTransient<ICityService, CityService>();
            services.AddTransient<IDistrictService, DistrictService>();
            services.AddTransient<IWardsService, WardsService>();
            services.AddTransient<IPropertyImageService, PropertyImageService>();
            services.AddTransient<INewsService, NewsService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IUserManagePostService, UserManagePostService>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IUserManageService, UserManageService>();
            services.AddTransient<IRoleService, RoleService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            /// Addlog

            loggerFactory.AddFile("Logs/easy2getroom-{Date}.txt");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}