﻿using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Data.Interfaces
{
    public interface ISwitchable
    {
        Status Status { get; set; }
    }
}
