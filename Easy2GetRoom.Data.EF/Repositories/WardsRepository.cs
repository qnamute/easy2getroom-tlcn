﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;

namespace Easy2GetRoom.Data.EF.Repositories
{
    public class WardsRepository : EFRepository<Wards, int>, IWardsRepository

    {
        public WardsRepository(AppDbContext context) : base(context)
        {

        }
    }
}