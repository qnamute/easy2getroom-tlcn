﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Easy2GetRoom.Extensions;
using Microsoft.AspNetCore.Authorization;
using Easy2GetRoom.Models;
using Easy2GetRoom.Application.Interfaces;
using AutoMapper;

namespace Easy2GetRoom.Controllers
{
    public class HomeController : Controller
    {

        private IPropertyCategoryService _propertyCategoryService;
        private IRentalTypeService _rentalTypeService;
        private IPropertyService _propertyService;
        private ICityService _cityService;
        private IDistrictService _districtService;
        private IWardsService _wardsServices;
        private INewsService _newsService;

        public HomeController(IPropertyCategoryService propertyCategoryService, IRentalTypeService rentalTypeService, IPropertyService propertyService
            , ICityService cityService, IDistrictService districtService, IWardsService wardsServices, INewsService newsService)
        {
            _propertyCategoryService = propertyCategoryService;
            _rentalTypeService = rentalTypeService;
            _propertyService = propertyService;
            _cityService = cityService;
            _districtService = districtService;
            _wardsServices = wardsServices;
            _newsService = newsService;
        }


        #region AJAX AIP
        public IActionResult Index()
        {
            var email = User.GetSpecificClaim("Email");

            var homeViewModel = new HomeViewModel();
            homeViewModel.LastestProperty = _propertyService.GetLastestProperties();
            homeViewModel.PropertyCategories = _propertyCategoryService.GetAll();
            homeViewModel.RentalTypes = _rentalTypeService.GetAll();
            homeViewModel.ListCities = _cityService.GetAll();
            homeViewModel.ListDistricts = _districtService.GetAll();
            homeViewModel.ListWards = _wardsServices.GetAll();
            homeViewModel.LastestNews = _newsService.GetLastestNews(3);

            return View(homeViewModel);
        }

        [HttpPost]
        public IActionResult GetgetDistrictbyCityId(int id)
        {
            var data = _districtService.GetDistrictbyCityId(id);
            return new OkObjectResult(data);
        }

        public IActionResult GetAll()
        {
            var data = _wardsServices.GetAll();
            return new OkObjectResult(data);
        }

        [HttpPost]
        public IActionResult GetWardsByDistrictId(int id)
        {
            var data = _wardsServices.GetWardsByDistrictId(id);
            return new OkObjectResult(data);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion
    }
}
