﻿using Easy2GetRoom.Application.ViewModels;
using System.Collections.Generic;

namespace Easy2GetRoom.Models.PropertyViewModels
{
    public class DetailViewModel
    {
        public PropertyViewModel Property { get; set; }

        public List<PropertyViewModel> RelatedProperties { get; set; }

        public PropertyCategoryViewModel Category { get; set; }

        public List<PropertyImageViewModel> PropertyImages { get; set; }
    }
}