﻿using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("FlaggedProperties")]
    public class FlaggedProperty : DomainEntity<int>, ISwitchable
    {
        public Guid UserId { get; set; }

        public int PropertyId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("PropertyId")]
        public virtual Property Property { get; set; }

        public Status Status { get; set; }
    }
}
