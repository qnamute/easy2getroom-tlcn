﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Data.IRepositories
{
    public interface IWardsRepository : IRepository<Wards, int>
    {
        
    }
}
