﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Easy2GetRoom.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class WardsController : BaseController
    {
        private IDistrictService _districService;
        private IWardsService _wardsService;
        public WardsController(IDistrictService districtService, IWardsService wardsService)
        {
            _districService = districtService;
            _wardsService = wardsService;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetWardsByDistrictId(int id)
        {
            var data = _wardsService.GetWardsByDistrictId(id);
            return new OkObjectResult(data);
        }
        
    }
}