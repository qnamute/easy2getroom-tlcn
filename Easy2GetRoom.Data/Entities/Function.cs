﻿using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Easy2GetRoom.Data.Entities
{
    [Table("Functions")]
    public class Function : DomainEntity<int>, ISwitchable, ISortable
    {
        public Function()
        {

        }
        public Function(int id, string name, string url, int parentId, string iconCss, int sortOrder)
        {
            this.Id = id;
            this.Name = name;
            this.URL = url;
            this.ParentId = parentId;
            this.IconCss = iconCss;
            this.SortOrder = sortOrder;
            this.Status = Status.Active;
        }

        public Function(string name, string url, int parentId, string iconCss, int sortOrder)
        {
            this.Name = name;
            this.URL = url;
            this.ParentId = parentId;
            this.IconCss = iconCss;
            this.SortOrder = sortOrder;
            this.Status = Status.Active;
        }
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string URL { get; set; }

        public int ParentId { get; set; }

        public string IconCss { get; set; }
        public int SortOrder { get; set; }
        public Status Status { get; set; }
    }
}