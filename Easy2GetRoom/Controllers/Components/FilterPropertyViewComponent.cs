﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy2GetRoom.Controllers.Components
{
    public class FilterPropertyViewComponent : ViewComponent
    {
        private IPropertyCategoryService _propertyCategoryService;
        private IRentalTypeService _rentalTypeService;
        private ICityService _cityService;

        public FilterPropertyViewComponent(IPropertyCategoryService propertyCategoryService,
            IRentalTypeService rentalTypeService, ICityService cityService)
        {
            _propertyCategoryService = propertyCategoryService;
            _rentalTypeService = rentalTypeService;
            _cityService = cityService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var filterViewModels = new FilterViewModel();
            filterViewModels.PropertyCategories = _propertyCategoryService.GetAll();
            filterViewModels.RentalTypes = _rentalTypeService.GetAll();
            filterViewModels.ListCities = _cityService.GetAll();

            return View(filterViewModels);
        }
    }
}
