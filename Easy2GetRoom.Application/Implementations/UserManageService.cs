﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Infrastructure.Interfaces;
using Easy2GetRoom.Utilities.Dtos;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace Easy2GetRoom.Application.Implementations
{
    public class UserManageService : IUserManageService
    {
        private UserManager<User> _userManager;
        private RoleManager<Role> _roleManager;
        private IUnitOfWork _unitOfWork;

        public UserManageService(UserManager<User> userManager, RoleManager<Role> roleManager, IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _unitOfWork = unitOfWork;
        }

        public void Add(User user)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            var user = _userManager.FindByIdAsync(id).Result;
            user.Status = Status.InActive;
            _userManager.UpdateAsync(user);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public PagedResult<User> GetAllPaging(Guid? role, string keyWord, int page, int pageSize)
        {
            var data = _userManager.Users.ToListAsync().Result;

            //if (role != null)
            //{
            //    data = data.
            //}

            int totalRow = data.Count;

            var paginationSet = new PagedResult<User>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };
            return paginationSet;
        }

        public User GetById(string id)
        {
            return _userManager.FindByIdAsync(id).Result;
        }

        public void Update(User user)
        {
            var model = _userManager.FindByIdAsync(user.Id.ToString()).Result;
            if (!string.IsNullOrEmpty(user.FullName))
            {
                model.FullName = user.FullName;
            }
            if (!string.IsNullOrEmpty(user.PhoneNumber))
            {
                model.PhoneNumber = user.PhoneNumber;
            }
            _userManager.UpdateAsync(model);
            _unitOfWork.Commit();
        }

        public void UpdateRole(User user, string role)
        {
            var model = _userManager.FindByIdAsync(user.Id.ToString()).Result;
            if(role.Equals("Admin"))
            {
                _userManager.RemoveFromRoleAsync(model, "Admin");
            }
            else
            {
                _userManager.RemoveFromRoleAsync(model, "User");
            }
            _userManager.AddToRoleAsync(model, role).Wait(); // add user register vao role user
            _unitOfWork.Commit();
        }
    }
}