﻿using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("NewsImages")]
    public class NewsImage : DomainEntity<int>, IDateTracking
    {
        public NewsImage(int id, string url, int newsId)
        {
            Id = id;
            Url = url;
            NewsId = newsId;
        }

        public NewsImage(int id, string url, int newsId, DateTime dateCreated, DateTime dateModified, News news)
        {
            Id = id;
            Url = url;
            NewsId = newsId;
            DateCreated = dateCreated;
            DateModified = dateModified;
            News = news;
        }

        [StringLength(255)]
        [Required]
        public string Url { get; set; }

        public int NewsId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        [ForeignKey("NewsId")]
        public virtual News News { get; set; }
    }
}
