﻿using Easy2GetRoom.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy2GetRoom.Models.UpPostViewmodels
{
    public class UpPostViewModel
    {
        public List<PropertyCategoryViewModel> Categories { get; set; }

        public List<RentalTypeViewModel> RentalTypes { get; set; }

        public List<CityViewModel> Cities { get; set; }
    }
}
