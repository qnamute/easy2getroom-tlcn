﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Utilities.Dtos
{
    public class Acreage
    {
        public string Name { get; set; }

        public int Value { get; set; }
    }
}
