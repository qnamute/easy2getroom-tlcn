﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Easy2GetRoom.Data.EF.Migrations
{
    public partial class initial09 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasSeen",
                table: "Messages");

            migrationBuilder.AddColumn<int>(
                name: "IsReply",
                table: "Messages",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsReply",
                table: "Messages");

            migrationBuilder.AddColumn<int>(
                name: "HasSeen",
                table: "Messages",
                nullable: false,
                defaultValue: 0);
        }
    }
}
