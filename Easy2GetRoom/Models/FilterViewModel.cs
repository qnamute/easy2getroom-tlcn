﻿using Easy2GetRoom.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy2GetRoom.Models
{
    public class FilterViewModel
    {
        public List<PropertyCategoryViewModel> PropertyCategories { get; set; }

        public List<RentalTypeViewModel> RentalTypes { get; set; }

        public List<CityViewModel> ListCities { get; set; }

        public List<DistrictViewModel> ListDistricts { get; set; }

        public List<WardsViewModel> ListWards { get; set; }
    }
}
