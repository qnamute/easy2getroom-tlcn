﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Easy2GetRoom.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            //Access to Claim
            return View();
        }
    }
}