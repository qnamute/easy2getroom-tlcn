﻿using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Easy2GetRoom.Data.Entities
{
    [Table("Messages")]
    public class Message : DomainEntity<int>, IDateTracking
    {
        public Message(int id, int parentId, Guid idSender, string emailSender, ReplyMessage isReply, string title, string content, DateTime dateCreated, DateTime dateModified)
        {
            Id = id;
            ParentId = parentId;
            IdSender = idSender;
            EmailSender = emailSender;
            IsReply = isReply;
            Title = title;
            Content = content;
            DateCreated = dateCreated;
            DateModified = dateModified;
        }

        public Message(int id, int parentId, Guid idSender, string emailSender, ReplyMessage isReply, string title, string content, DateTime dateCreated, DateTime dateModified, User userSender)
        {
            Id = id;
            ParentId = parentId;
            IdSender = idSender;
            EmailSender = emailSender;
            IsReply = isReply;
            Title = title;
            Content = content;
            DateCreated = dateCreated;
            DateModified = dateModified;
            UserSender = userSender;
        }

        public int ParentId { get; set; }

        public Guid IdSender { get; set; }

        public string EmailSender { get; set; }

        //public Guid IdReceiver { get; set; }

        public ReplyMessage IsReply { get; set; }

        [StringLength(200)]
        [Required]
        public string Title { get; set; }

        [StringLength(1000)]
        [Required]
        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        [ForeignKey("IdSender")]
        public virtual User UserSender { get; set; }

        //[ForeignKey("IdReceiver")]
        //public virtual User UserReceiver { get; set; }
    }
}