﻿using Easy2GetRoom.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IPropertyCategoryService
    {
        PropertyCategoryViewModel Add(PropertyCategoryViewModel propertyCategoryViewModal);

        void Update(PropertyCategoryViewModel propertyCategoryViewModal);

        void Delete(int id);

        List<PropertyCategoryViewModel> GetAll();

        List<PropertyCategoryViewModel> GetAll(string keyword);

        PropertyCategoryViewModel GetById(int id);

        void Save();
    }
}
