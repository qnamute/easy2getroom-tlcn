﻿using Easy2GetRoom.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Utilities.Constants
{
    public static class CommonConstantsProperty
    {
        //Property Category
        public const int Houses = 1;
        public const int Apartments = 2;
        public const int Rooms = 3;
        //RentalType
        public const int ForRent = 1;
        public const int NeedRent = 2;
        public const int ForStayWith = 3;
        public const int NeedStayWith = 4;

        //Acreage 

    }
}
