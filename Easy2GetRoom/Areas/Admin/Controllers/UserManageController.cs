﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Areas.Admin.Models;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class UserManageController : BaseController
    {
        private IUserManageService _userManageService;
        private IRoleService _roleService;
        private IUnitOfWork _unitOfWork;

        public UserManageController(IUserManageService userManageService, IRoleService roleService, IUnitOfWork unitOfWork)
        {
            _userManageService = userManageService;
            _roleService = roleService;
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region AJAX API

        public IActionResult GetAllPaging(Guid? role, string keyWord, int page, int pageSize)
        {
            var model = _userManageService.GetAllPaging(role, keyWord, page, pageSize);
            var result = new UserViewModel();
            result.Users = model;
            List<String> lstRoles = new List<String>();
            foreach (var item in model.Results)
            {
                var x = _roleService.GetRoles(item).FirstOrDefault();
                lstRoles.Add(x);
            }
            result.Roles = lstRoles;
            return new OkObjectResult(result);
        }

        public IActionResult GetById(string id)
        {
            var result = new UserViewModel();
            var user = _userManageService.GetById(id);
            result.User = _userManageService.GetById(id);
            result.Role = _roleService.GetRoles(user).FirstOrDefault();
            return new OkObjectResult(result);
        }

        public IActionResult SaveEntity(User user)
        {
            _userManageService.Update(user);
            //
            return new OkResult();
        }

        public IActionResult Delete(string id)
        {
            _userManageService.Delete(id);
            _unitOfWork.Commit();
            return new OkResult();
        }

        public IActionResult GetRoles()
        {
            var model = _roleService.GetAll();
            return new OkObjectResult(model);
        }

        public IActionResult UpdateRole(User user, string role)
        {
            _userManageService.UpdateRole(user, role);
            return new OkResult();
        }
        #endregion
    }
}