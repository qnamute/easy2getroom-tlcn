﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using Easy2GetRoom.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Application.Implementations
{
    public class NewsService : INewsService
    {
        private INewsRepository _newsRepository;
        private IUnitOfWork _unitOfWork;

        public NewsService(INewsRepository newsRepository, IUnitOfWork unitOfWork)
        {
            _newsRepository = newsRepository;
            _unitOfWork = unitOfWork;
        }

        public NewsViewModel Add(NewsViewModel newsViewModel)
        {
            var news = Mapper.Map<NewsViewModel, News>(newsViewModel);

            _newsRepository.Add(news);
            _unitOfWork.Commit();

            return Mapper.Map<News, NewsViewModel>(news);
        }

        public void Delete(int id)
        {
            _newsRepository.Remove(id);
            _unitOfWork.Commit();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<NewsViewModel> GetAll()
        {
            return _newsRepository.FindAll(x => x.NewsImages, x => x.User).ProjectTo<NewsViewModel>().ToList();
        }

        public PagedResult<NewsViewModel> GetAllPaging(string keyWord, int page, int pageSize)
        {
            var query = _newsRepository.FindAll(x => x.User);
            if (!string.IsNullOrEmpty(keyWord))
            {
                query = query.Where(x => x.Title.Contains(keyWord) || x.Content.Contains(keyWord));
            }

            int totalRow = query.Count();

            query = query.Skip((page - 1) * pageSize).Take(pageSize);

            var data = query.ProjectTo<NewsViewModel>().ToList();

            var paginationSet = new PagedResult<NewsViewModel>()
            {
                Results = data,
                CurrentPage = page,
                RowCount = totalRow,
                PageSize = pageSize
            };

            return paginationSet;
        }

        public NewsViewModel GetById(int id)
        {
            return Mapper.Map<News, NewsViewModel>(_newsRepository.FindById(id, x=>x.User));
        }

        public List<NewsViewModel> GetLastestNews(int top)
        {
            var data = _newsRepository.FindAll(x => x.NewsImages, x => x.User).Take(top).ProjectTo<NewsViewModel>().ToList();
            return data;
        }

        public void Update(NewsViewModel newsViewModel)
        {
            var news = Mapper.Map<NewsViewModel, News>(newsViewModel);
            _newsRepository.Update(news);
            _unitOfWork.Commit();
        }
    }
}