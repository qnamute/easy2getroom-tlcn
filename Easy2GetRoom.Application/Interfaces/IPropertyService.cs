﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Utilities.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IPropertyService : IDisposable
    {
        List<PropertyViewModel> GetAll();

        PagedResult<PropertyViewModel> GetAllPaging(int? categoryId, string keyWord, int page, int pageSize);

        PagedResult<PropertyViewModel> GetAllActivePropertyPaging(int? categoryId, string keyWord, int page, int pageSize, string userId);

        PagedResult<PropertyViewModel> GetAllInActivePropertyPaging(int? categoryId, string keyWord, int page, int pageSize, string userId);

        PagedResult<PropertyViewModel> GetAllAwaitingApprovalPropertyPaging(int? categoryId, string keyWord, int page, int pageSize, string userId);
        PropertyViewModel GetById(int id);

        List<PropertyViewModel> GetAllSlideProperty();

        void UpdateStatus(int id, Status status);

        void UpdateSlideFlag(int id, int isFlag);

        void Delete(int id);

        void Update(PropertyViewModel propertyViewModel);

        void Save();

        void AddImages(string[] imagesUrl, int propertyId);

        PropertyViewModel Add(PropertyViewModel propertyViewModel);

        UserViewModel GetPropertyOwnerByPropertyId(int id);

        List<PropertyViewModel> GetLastestProperties();

        PagedResult<PropertyViewModel> GetFilterPaging(int? categoryId, int? rentalType, int? cityId, int? districtId,
            int? wardsId, int? acreage, int? price, int page, int pageSize);

        List<PropertyViewModel> GetRelatedProperties(int id, int top);
    }
}
