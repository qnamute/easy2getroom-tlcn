﻿var newsDetailController = function () {
    this.initialize = function () {
        registerControls();
        registerEvent();
        loadDescription();
    }
    function registerEvent() {

    }

    function registerControls() {
        CKEDITOR.replace('txtContentTemp');

        // Fix ckeditor link
        // Fix: cannot click on element ck in modal
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
            $(document)
                .off('focusin.bs.modal') // guard against infinite focus loop
                .on('focusin.bs.modal', $.proxy(function (e) {
                    if (
                        this.$element[0] !== e.target && !this.$element.has(e.target).length
                        // CKEditor compatibility fix start.
                        && !$(e.target).closest('.cke_dialog, .cke').length
                        // CKEditor compatibility fix end.
                    ) {
                        this.$element.trigger('focus');
                    }
                }, this));
        };
    }

    function loadDescription() {
        var des = CKEDITOR.instances.txtContentTemp.getData();
        console.log(des);
        $('#txtContent').html(des);
        CKEDITOR.instances.txtContentTemp.destroy();
    }
}