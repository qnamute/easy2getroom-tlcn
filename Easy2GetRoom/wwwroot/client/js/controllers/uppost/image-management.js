﻿var ImageManagement = function () {
    var self = this;
    var parent = parent;

    var images = [];

    this.initialize = function () {
        registerEvents();
    }

    function registerEvents() {
        $('body').on('click', '.btn-images', function (e) {
            e.preventDefault();
            var that = $(this).data('id');
            $('#hidId').val(that);
            clearFileInput($("#fileImage"));
            loadImages();
            $('#modal-image-manage').modal('show');
        });

        $("#btnSaveImages").on('click', function () {
            var imageList = [];
            $.each($('#image-list').find('img'), function (i, item) {
                imageList.push($(this).data('path'));
            });
            $.ajax({
                url: '/admin/Product/SaveImages',
                data: {
                    productId: $('#hidId').val(),
                    images: images
                },
                type: 'post',
                dataType: 'json',
                success: function (response) {
                    $('#modal-image-manage').modal('hide');
                    $('#image-list').html('');
                    clearFileInput($("#fileImage"));

                    $('#lbNotifyTitle').html('Đăng tải hình ảnh thành công !');
                    $('#btnSubmitModal').html('Tiếp');
                    $('#btnSubmitModal').addClass('wizard-next');
                    $('#btnSubmitModal').removeClass('wizard-finish');
                    $('#notifyModal').modal('show');
                },
                error: function (status) {
                    console.log(status);
                    $('#notifyModal').modal('show');
                    $('#lbNotifyTitle').html('Đăng tải hình ảnh không thành công !');
                    $('#btnSubmitModal').html('Đóng');
                }
            });
        });
    }
    function loadImages() {
        $.ajax({
            url: '/admin/Product/GetImages',
            data: {
                productId: $('#hidId').val()
            },
            type: 'get',
            dataType: 'json',
            success: function (response) {
                var render = '';
                $.each(response, function (i, item) {
                    render += '<div class="col-md-3"><img width="100" src="' + item.Path + '"><br/><a href="#" class="btn-delete-image">Xóa</a></div>'
                });
                $('#image-list').html(render);
                easy2getroom.notify('Đăng hình thành công !', 'success');
                clearFileInput();
            }
        });
    }

    function clearFileInput(ctrl) {
        //try {
        //    ctrl.value = null;
        //} catch (ex) { }
        //if (ctrl.value) {
        //    ctrl.parentNode.replaceChild(ctrl.cloneNode(true), ctrl);
        //}
    }
}