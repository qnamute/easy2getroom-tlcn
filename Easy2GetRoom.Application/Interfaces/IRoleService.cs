﻿using Easy2GetRoom.Data.Entities;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IRoleService : IDisposable
    {
        List<Role> GetAll();

        List<string> GetRoles(User user);
    }
}