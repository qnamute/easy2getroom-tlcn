﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class PropertyController : BaseController
    {
        private IPropertyService _propertyService;
        private IPropertyCategoryService _propertyCategoryService;

        public PropertyController(IPropertyService propertyService, IPropertyCategoryService propertyCategoryService)
        {
            _propertyService = propertyService;
            _propertyCategoryService = propertyCategoryService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        #region AJAX API

        [HttpGet]
        public IActionResult GetAll()
        {
            var model = _propertyService.GetAll();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllCategories()
        {
            var model = _propertyCategoryService.GetAll();
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetAllPaging(int? categoryId, string keyWord, int page, int pageSize)
        {
            var model = _propertyService.GetAllPaging(categoryId, keyWord, page, pageSize);
            return new OkObjectResult(model);
        }

        [HttpGet]
        public IActionResult GetById(int id)
        {
            var model = _propertyService.GetById(id);
            return new OkObjectResult(model);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            _propertyService.Delete(id);
            _propertyService.Save();

            return new OkResult();
        }

        public IActionResult Update(int id, Status status)
        {
            PropertyViewModel property = _propertyService.GetById(id);
            property.Status = status;
            _propertyService.Update(property);
            _propertyService.Save();
            return new OkResult();
        }

        [HttpPost]
        public IActionResult SaveEntity(PropertyViewModel propertyViewModel)
        {
            if (!ModelState.IsValid)
            {
                IEnumerable<ModelError> allErrors = ModelState.Values.SelectMany(v => v.Errors);
                return new BadRequestObjectResult(allErrors);
            }
            else
            {
                if (propertyViewModel.Id == 0)
                {
                    //Add new property
                }
                else
                {
                    //Update property
                    _propertyService.Update(propertyViewModel);
                }
                _propertyService.Save();

                return new OkObjectResult(propertyViewModel);
            }
        }

        public IActionResult SetSlideFlag(int id, int isFlag)
        {
            _propertyService.UpdateSlideFlag(id, isFlag);
            _propertyService.Save();
            return new OkResult();
        }
        #endregion AJAX API
    }
}