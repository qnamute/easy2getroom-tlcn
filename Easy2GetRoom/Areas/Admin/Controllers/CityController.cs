﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Easy2GetRoom.Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class CityController : BaseController
    {
        private ICityService _cityService;
        public CityController(ICityService cityservice)
        {
            _cityService = cityservice;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GetAllCity()
        {
            var model = _cityService.GetAll();
            return new OkObjectResult(model);
        }
    }
}