﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Application.Implementations
{
    public class RoleService : IRoleService
    {
        private RoleManager<Role> _roleManager;
        private UserManager<User> _userManager;
        private IUnitOfWork _unitOfWork;

        public RoleService(RoleManager<Role> roleManager,UserManager<User> userManager, IUnitOfWork unitOfWork)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<Role> GetAll()
        {
            return _roleManager.Roles.ToListAsync().Result;
        }

        public List<string> GetRoles(User user)
        {
            return _userManager.GetRolesAsync(user).Result.ToList();
        }
    }
}