﻿using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.ViewModels
{
    public class PropertyViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int PropertyCategoryId { get; set; }

        public int CityId { get; set; }

        public int DistrictId { get; set; }

        public int WardsId { get; set; }

        public int RentalTypeId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public decimal PriceFrom { get; set; }

        public decimal PriceTo { get; set; }

        public float Acreage { get; set; }

        public float AcreageFrom { get; set; }

        public float AcreageTo { get; set; }

        public string Address { get; set; }

        public double? Lat { get; set; }

        public double? Lng { get; set; }

        public Status Status { get; set; }

        public int SlideFlag { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public Guid? UserId { get; set; }

        public PropertyCategoryViewModel PropertyCategory { get; set; }

        public CityViewModel City { get; set; }

        public DistrictViewModel District { get; set; }

        public WardsViewModel Wards { get; set; }

        public RentalTypeViewModel RentalType { get; set; }

        public UserViewModel User { get; set; } 

        public List<PropertyImageViewModel> PropertyImages { get; set; }
    }
}