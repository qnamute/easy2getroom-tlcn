﻿using Easy2GetRoom.Application.ViewModels;
using System.Collections.Generic;

namespace Easy2GetRoom.Models
{
    public class HomeViewModel
    {
        public List<PropertyViewModel> LastestProperty { get; set; }

        public List<PropertyCategoryViewModel> PropertyCategories { get; set; }

        public List<RentalTypeViewModel> RentalTypes { get; set; }

        public List<CityViewModel> ListCities { get; set; }

        public List<DistrictViewModel> ListDistricts { get; set; }

        public List<WardsViewModel> ListWards { get; set; }

        public List<NewsViewModel> LastestNews { get; set; }
    }
}