﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.ViewModels
{
    public class NewsViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public Guid UserId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public UserViewModel User { get; set; }

        public List<NewsImageViewModel> NewsImages { get; set; }
    }
}
