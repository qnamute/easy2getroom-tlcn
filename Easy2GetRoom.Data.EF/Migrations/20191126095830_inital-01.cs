﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Easy2GetRoom.Data.EF.Migrations
{
    public partial class inital01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "News");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OwnerId",
                table: "News",
                nullable: false,
                defaultValue: 0);
        }
    }
}
