﻿using AutoMapper;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Entities;
using Microsoft.AspNetCore.Identity;
using System;

namespace Easy2GetRoom.Application.Implementations
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;

        public UserService(UserManager<User> userManager)
        {
            _userManager = userManager;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public UserViewModel GetUser(string userId)
        {
            var user = _userManager.FindByIdAsync(userId).Result;
            UserViewModel userViewModel = new UserViewModel();
            userViewModel = Mapper.Map<User, UserViewModel>(user);
            return userViewModel;
        }

        public void UpdateAvatar()
        {
            throw new NotImplementedException();
        }

        public void UpdateInfo(UserViewModel userViewModel)
        {
            var user = Mapper.Map<UserViewModel, User>(userViewModel);
            _userManager.UpdateAsync(user);            
        }

        public void UpdatePassword()
        {
            throw new NotImplementedException();
        }
    }
}