-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: easy2getroom
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AnnouncementUsers`
--

DROP TABLE IF EXISTS `AnnouncementUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AnnouncementUsers` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `AnnouncementId` int(11) NOT NULL,
  `UserId` char(36) NOT NULL,
  `HasRead` bit(1) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_AnnouncementUsers_AnnouncementId` (`AnnouncementId`),
  KEY `IX_AnnouncementUsers_UserId` (`UserId`),
  CONSTRAINT `FK_AnnouncementUsers_Announcements_AnnouncementId` FOREIGN KEY (`AnnouncementId`) REFERENCES `Announcements` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_AnnouncementUsers_Users_UserId` FOREIGN KEY (`UserId`) REFERENCES `Users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnnouncementUsers`
--

LOCK TABLES `AnnouncementUsers` WRITE;
/*!40000 ALTER TABLE `AnnouncementUsers` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnouncementUsers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Announcements`
--

DROP TABLE IF EXISTS `Announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Announcements` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `UserId` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Announcements`
--

LOCK TABLES `Announcements` WRITE;
/*!40000 ALTER TABLE `Announcements` DISABLE KEYS */;
/*!40000 ALTER TABLE `Announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AppRoleClaims`
--

DROP TABLE IF EXISTS `AppRoleClaims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AppRoleClaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` char(36) NOT NULL,
  `ClaimType` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `ClaimValue` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AppRoleClaims`
--

LOCK TABLES `AppRoleClaims` WRITE;
/*!40000 ALTER TABLE `AppRoleClaims` DISABLE KEYS */;
/*!40000 ALTER TABLE `AppRoleClaims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AppUserClaims`
--

DROP TABLE IF EXISTS `AppUserClaims`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AppUserClaims` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` char(36) NOT NULL,
  `ClaimType` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `ClaimValue` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AppUserClaims`
--

LOCK TABLES `AppUserClaims` WRITE;
/*!40000 ALTER TABLE `AppUserClaims` DISABLE KEYS */;
/*!40000 ALTER TABLE `AppUserClaims` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AppUserLogins`
--

DROP TABLE IF EXISTS `AppUserLogins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AppUserLogins` (
  `UserId` char(36) NOT NULL,
  `LoginProvider` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `ProviderKey` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `ProviderDisplayName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AppUserLogins`
--

LOCK TABLES `AppUserLogins` WRITE;
/*!40000 ALTER TABLE `AppUserLogins` DISABLE KEYS */;
INSERT INTO `AppUserLogins` VALUES ('08d773fa-0f62-f122-f3f8-fda68e243492','Facebook','1272898866245234','Facebook'),('08d773fe-0b8d-981a-9a0b-6784d48c81ed','Google','110805439551244353461','Google');
/*!40000 ALTER TABLE `AppUserLogins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AppUserRoles`
--

DROP TABLE IF EXISTS `AppUserRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AppUserRoles` (
  `UserId` char(36) NOT NULL,
  `RoleId` char(36) NOT NULL,
  PRIMARY KEY (`RoleId`,`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AppUserRoles`
--

LOCK TABLES `AppUserRoles` WRITE;
/*!40000 ALTER TABLE `AppUserRoles` DISABLE KEYS */;
INSERT INTO `AppUserRoles` VALUES ('08d773f4-721a-63d5-985d-5c0740864687','08d773f4-71cb-576f-734c-eddeb8fcba36');
/*!40000 ALTER TABLE `AppUserRoles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AppUserTokens`
--

DROP TABLE IF EXISTS `AppUserTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AppUserTokens` (
  `UserId` char(36) NOT NULL,
  `LoginProvider` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AppUserTokens`
--

LOCK TABLES `AppUserTokens` WRITE;
/*!40000 ALTER TABLE `AppUserTokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `AppUserTokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cities`
--

DROP TABLE IF EXISTS `Cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Cities` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cities`
--

LOCK TABLES `Cities` WRITE;
/*!40000 ALTER TABLE `Cities` DISABLE KEYS */;
INSERT INTO `Cities` VALUES (1,'Thành Phố Hồ Chí Minh'),(2,'Thành Phố Hà Nội'),(3,'Thành Phố Đà Nẵng'),(4,'Tỉnh Thừa Thiên Huế'),(5,'Tỉnh Đăk Nông'),(6,'Thành Phố Buôn Mê Thuật'),(7,'Thành Phố Bình Dương'),(8,'Tỉnh Bình Định'),(9,'Tỉnh Long An'),(10,'Tỉnh Thanh Hóa'),(11,'Tỉnh Nghệ An'),(12,'Thành Phố Cần Thơ'),(13,'Tỉnh Đồng Nai'),(14,'Tỉnh Cà Mau'),(15,'Tỉnh Thái Bình');
/*!40000 ALTER TABLE `Cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Districts`
--

DROP TABLE IF EXISTS `Districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Districts` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CityId` int(11) NOT NULL,
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_Districts_CityId` (`CityId`),
  CONSTRAINT `FK_Districts_Cities_CityId` FOREIGN KEY (`CityId`) REFERENCES `Cities` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Districts`
--

LOCK TABLES `Districts` WRITE;
/*!40000 ALTER TABLE `Districts` DISABLE KEYS */;
INSERT INTO `Districts` VALUES (1,1,'Quận 1'),(2,1,'Huyện Cần Giờ'),(3,1,'Huyện Nhà Bè'),(4,1,'Huyện Bình Chánh'),(5,1,'Quận Tân Phú'),(6,1,'Quận Tân Bình'),(7,1,'Quận Bình Tân'),(8,1,'Quận Gò Vấp'),(9,1,'Quận Thủ Đức'),(10,1,'Quận Bình Thạnh'),(11,1,'Huyện Hóc Môn'),(12,1,'Quận 12'),(13,1,'Quận 10'),(14,1,'Quận 9'),(15,1,'Quận 8'),(16,1,'Quận 7'),(17,1,'Quận 6'),(18,1,'Quận 5'),(19,1,'Quận 4'),(20,1,'Quận 3'),(21,1,'Quận 2'),(22,1,'Quận 11'),(23,1,'Huyện Củ Chi');
/*!40000 ALTER TABLE `Districts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FlaggedProperties`
--

DROP TABLE IF EXISTS `FlaggedProperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FlaggedProperties` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` char(36) NOT NULL,
  `PropertyId` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_FlaggedProperties_PropertyId` (`PropertyId`),
  KEY `IX_FlaggedProperties_UserId` (`UserId`),
  CONSTRAINT `FK_FlaggedProperties_Properties_PropertyId` FOREIGN KEY (`PropertyId`) REFERENCES `Properties` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FlaggedProperties_Users_UserId` FOREIGN KEY (`UserId`) REFERENCES `Users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FlaggedProperties`
--

LOCK TABLES `FlaggedProperties` WRITE;
/*!40000 ALTER TABLE `FlaggedProperties` DISABLE KEYS */;
/*!40000 ALTER TABLE `FlaggedProperties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Functions`
--

DROP TABLE IF EXISTS `Functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Functions` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `URL` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `ParentId` int(11) NOT NULL,
  `IconCss` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `SortOrder` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Functions`
--

LOCK TABLES `Functions` WRITE;
/*!40000 ALTER TABLE `Functions` DISABLE KEYS */;
INSERT INTO `Functions` VALUES (1,'Quản lý bài đăng','',0,'fa-tasks',1,1),(2,'Tất cả bài đăng','/Admin/Property',1,'fa-home',1,1),(3,'Hoạt động','/Admin/ActiveProperty',1,'fa-home',2,1),(4,'Tạm ẩn','/Admin/InActiveProperty',1,'fa-home',3,1),(5,'Chờ phê duyệt','/Admin/AwaitingApprovalProperty',1,'fa-home',4,1),(6,'Tin nhắn','/Admin/Messages',0,'fa-comments',2,1),(7,'Tin tức','/Admin/News',0,'fa-paper-plane',3,1),(8,'Thông báo','/Admin/Announcements',0,'fa-bell',4,1),(9,'Quản lý vị trí','/Admin/ManagePlace',0,'fa-map-marker',5,1),(11,'Tỉnh/Thành phố','/Admin/City',9,'fa-building',7,1),(12,'Quận/Huyện','/Admin/District',9,'fa-chevron-down',8,1),(13,'Phường/Xã/Thị trấn','/Admin/Wards',9,'fa-clone',9,1);
/*!40000 ALTER TABLE `Functions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MessageImages`
--

DROP TABLE IF EXISTS `MessageImages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `MessageImages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Url` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `MessageId` int(11) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_MessageImages_MessageId` (`MessageId`),
  CONSTRAINT `FK_MessageImages_Messages_MessageId` FOREIGN KEY (`MessageId`) REFERENCES `Messages` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MessageImages`
--

LOCK TABLES `MessageImages` WRITE;
/*!40000 ALTER TABLE `MessageImages` DISABLE KEYS */;
/*!40000 ALTER TABLE `MessageImages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Messages`
--

DROP TABLE IF EXISTS `Messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Messages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `IdSender` char(36) NOT NULL,
  `IdReceiver` char(36) NOT NULL,
  `Content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_Messages_IdReceiver` (`IdReceiver`),
  KEY `IX_Messages_IdSender` (`IdSender`),
  CONSTRAINT `FK_Messages_Users_IdReceiver` FOREIGN KEY (`IdReceiver`) REFERENCES `Users` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Messages_Users_IdSender` FOREIGN KEY (`IdSender`) REFERENCES `Users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Messages`
--

LOCK TABLES `Messages` WRITE;
/*!40000 ALTER TABLE `Messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `Messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `News`
--

DROP TABLE IF EXISTS `News`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `News` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `UserId` char(36) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_News_UserId` (`UserId`),
  CONSTRAINT `FK_News_Users_UserId` FOREIGN KEY (`UserId`) REFERENCES `Users` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `News`
--

LOCK TABLES `News` WRITE;
/*!40000 ALTER TABLE `News` DISABLE KEYS */;
INSERT INTO `News` VALUES (1,'Bộ Xây dựng lần đầu tiên công bố thông tin số liệu về thị trường bất động sản','<h2>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</h2>\n\n<p><img alt=\"\" src=\"/uploaded/images/20191129/cauthuthiem2-15625729911621618245651-crop-1574925756714994393469.jpg\" style=\"height:400px; width:100%\" /></p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:38:20.657678','2019-11-29 02:38:20.657706'),(2,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<h2>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</h2>\n\n<p><img alt=\"\" src=\"/uploaded/images/20191129/cauthuthiem2-15625729911621618245651-crop-1574925756714994393469.jpg\" style=\"height:400px; width:100%\" /></p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:38:48.970032','2019-11-29 02:38:48.970033'),(3,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:38:56.265494','2019-11-29 02:38:56.265495'),(4,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:00.177053','2019-11-29 02:39:00.177054'),(5,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:03.927952','2019-11-29 02:39:03.927952'),(6,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:07.864826','2019-11-29 02:39:07.864828'),(7,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:12.840921','2019-11-29 02:39:12.840921'),(8,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:15.998513','2019-11-29 02:39:15.998514'),(9,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:18.789012','2019-11-29 02:39:18.789012'),(10,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:23.864852','2019-11-29 02:39:23.864853'),(11,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:27.333743','2019-11-29 02:39:27.333744'),(12,'Bộ Xây dựng lần đầu tiên công bố số liệu chính thức về thị trường bất động sản quý 3 năm 2019. Số liệu được Bộ Xây dựng tổng hợp từ 22 Sở Xây dựng trên cả nước.','<p>Bộ X&acirc;y dựng lần đầu ti&ecirc;n c&ocirc;ng bố số liệu ch&iacute;nh thức về thị trường bất động sản qu&yacute; 3 năm 2019. Số liệu được Bộ X&acirc;y dựng tổng hợp từ 22 Sở X&acirc;y dựng tr&ecirc;n cả nước.</p>\n','08d773f4-721a-63d5-985d-5c0740864687','2019-11-29 02:39:39.505402','2019-11-29 02:39:39.505403');
/*!40000 ALTER TABLE `News` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NewsImages`
--

DROP TABLE IF EXISTS `NewsImages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `NewsImages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NewsId` int(11) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_NewsImages_NewsId` (`NewsId`),
  CONSTRAINT `FK_NewsImages_News_NewsId` FOREIGN KEY (`NewsId`) REFERENCES `News` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NewsImages`
--

LOCK TABLES `NewsImages` WRITE;
/*!40000 ALTER TABLE `NewsImages` DISABLE KEYS */;
/*!40000 ALTER TABLE `NewsImages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Properties`
--

DROP TABLE IF EXISTS `Properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Properties` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `PropertyCategoryId` int(11) NOT NULL,
  `CityId` int(11) NOT NULL,
  `DistrictId` int(11) NOT NULL,
  `WardsId` int(11) NOT NULL,
  `RentalTypeId` int(11) NOT NULL,
  `Title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Price` decimal(65,30) NOT NULL,
  `PriceFrom` decimal(65,30) NOT NULL,
  `PriceTo` decimal(65,30) NOT NULL,
  `Acreage` float NOT NULL,
  `AcreageFrom` float NOT NULL,
  `AcreageTo` float NOT NULL,
  `Address` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Lat` double DEFAULT NULL,
  `Lng` double DEFAULT NULL,
  `Status` int(11) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  `UserId` char(36) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_Properties_CityId` (`CityId`),
  KEY `IX_Properties_DistrictId` (`DistrictId`),
  KEY `IX_Properties_PropertyCategoryId` (`PropertyCategoryId`),
  KEY `IX_Properties_RentalTypeId` (`RentalTypeId`),
  KEY `IX_Properties_UserId` (`UserId`),
  KEY `IX_Properties_WardsId` (`WardsId`),
  CONSTRAINT `FK_Properties_Cities_CityId` FOREIGN KEY (`CityId`) REFERENCES `Cities` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Properties_Districts_DistrictId` FOREIGN KEY (`DistrictId`) REFERENCES `Districts` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Properties_PropertyCategories_PropertyCategoryId` FOREIGN KEY (`PropertyCategoryId`) REFERENCES `PropertyCategories` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Properties_RentalTypes_RentalTypeId` FOREIGN KEY (`RentalTypeId`) REFERENCES `RentalTypes` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_Properties_Users_UserId` FOREIGN KEY (`UserId`) REFERENCES `Users` (`Id`) ON DELETE RESTRICT,
  CONSTRAINT `FK_Properties_Wards_WardsId` FOREIGN KEY (`WardsId`) REFERENCES `Wards` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Properties`
--

LOCK TABLES `Properties` WRITE;
/*!40000 ALTER TABLE `Properties` DISABLE KEYS */;
INSERT INTO `Properties` VALUES (1,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,1,'2019-11-28 18:16:42.842649','2019-11-28 18:16:42.842752','08d773f4-721a-63d5-985d-5c0740864687'),(2,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,1,'2019-11-28 18:16:42.842833','2019-11-28 18:16:42.842841','08d773f4-721a-63d5-985d-5c0740864687'),(3,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,1,'2019-11-28 18:16:42.842843','2019-11-28 18:16:42.842843','08d773f4-721a-63d5-985d-5c0740864687'),(4,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,1,'2019-11-28 18:16:42.842843','2019-11-28 18:16:42.842843','08d773f4-721a-63d5-985d-5c0740864687'),(5,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,1,'2019-11-28 18:16:42.842843','2019-11-28 18:16:42.842843','08d773f4-721a-63d5-985d-5c0740864687'),(6,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,1,'2019-11-28 18:16:42.842844','2019-11-28 18:16:42.842844','08d773f4-721a-63d5-985d-5c0740864687'),(7,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,0,'2019-11-28 18:16:42.842844','2019-11-28 18:16:42.842844','08d773f4-721a-63d5-985d-5c0740864687'),(8,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,0,'2019-11-28 18:16:42.842844','2019-11-28 18:16:42.842844','08d773f4-721a-63d5-985d-5c0740864687'),(9,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,0,'2019-11-28 18:16:42.842844','2019-11-28 18:16:42.842844','08d773f4-721a-63d5-985d-5c0740864687'),(10,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,0,'2019-11-28 18:16:42.842845','2019-11-28 18:16:42.842845','08d773f4-721a-63d5-985d-5c0740864687'),(11,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,0,'2019-11-28 18:16:42.842845','2019-11-28 18:16:42.842845','08d773f4-721a-63d5-985d-5c0740864687'),(12,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,2,'2019-11-28 18:16:42.842845','2019-11-28 18:16:42.842845','08d773f4-721a-63d5-985d-5c0740864687'),(13,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,2,'2019-11-28 18:16:42.842845','2019-11-28 18:16:42.842845','08d773f4-721a-63d5-985d-5c0740864687'),(14,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,2,'2019-11-28 18:16:42.842846','2019-11-28 18:16:42.842846','08d773f4-721a-63d5-985d-5c0740864687'),(15,'Phòng trọ cho thuê',3,1,14,8,1,'Phòng trỌ thủ đức 20m2','Phòng trọ Thủ Đức, rộng rãi',2500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,25,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,2,'2019-11-28 18:16:42.842846','2019-11-28 18:16:42.842846','08d773f4-721a-63d5-985d-5c0740864687'),(16,'Phòng trọ rộng rãi',3,1,1,14,4,'Phòng trọ Quận 1 diện tích 20 mét vuông','<p>- R&ocirc;nnj r&atilde;i</p>\n\n<p>Tho&aacute;ng m&aacute;t</p>\n\n<p><q><em><strong>zxczxc</strong></em></q></p>\n\n<p><q><em><strong><img alt=\"\" src=\"/uploaded/images/20191129/img_1.jpg\" style=\"height:500px; width:500px\" /></strong></em></q></p>\n',1500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,20,0,0,'111/6 Đặng Thùy Trâm',10.8497386,106.7685109,1,'2019-11-29 02:26:18.947953','2019-11-29 02:26:31.669290','08d773f4-721a-63d5-985d-5c0740864687'),(17,'cczcxz',1,1,1,14,4,'Nhà ở Quận 1 diện tích 20 mét vuông','<p><em><strong>- zxc</strong></em></p>\n\n<p>&nbsp;</p>\n\n<p><em><strong>zc</strong></em></p>\n\n<p><em><strong>zx</strong></em></p>\n\n<p><em><strong>czx--zc</strong></em></p>\n\n<p><em><strong>z</strong></em></p>\n\n<p><em><strong>xc</strong></em></p>\n\n<p><em><strong>z</strong></em></p>\n',1600000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,20,0,0,'111/6 dasdasd',10.8497386,106.7685109,1,'2019-12-01 11:52:01.197358','2019-12-01 11:53:24.037631','08d773fa-0f62-f122-f3f8-fda68e243492'),(18,'Chung Cư Cửu Long',2,1,1,13,4,'Chung cư Quận 1 diện tích 19 mét vuông','<p>Kh&ocirc;ng c&oacute; g&igrave; để m&ocirc; tả</p>\n',1600000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,19,0,0,'111/6 Đặng Thùy Trâm',11.438829,106.6173415,1,'2019-12-02 22:23:27.565387','2019-12-02 22:23:47.441658','08d773fa-0f62-f122-f3f8-fda68e243492'),(19,'Nhà nguyên căn Thủ Đức',1,1,9,16,2,'Nhà ở Quận Thủ Đức diện tích 40 mét vuông','<p>R&ocirc;ng r&atilde;i</p>\n\n<p>Tho&aacute;ng m&aacute;t</p>\n\n<p>kh&ocirc;ng chung chủ</p>\n\n<p>li&ecirc;n hệ</p>\n\n<p>&nbsp;</p>\n',5000000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,40,0,0,'01 Võ Văn Ngân',10.8292489,106.7685109,1,'2019-12-02 22:26:39.518459','2019-12-02 22:26:44.764462','08d773f4-721a-63d5-985d-5c0740864687'),(20,'Phòng trọ cho thuê',3,1,1,13,4,'Phòng trọ Quận 1 diện tích 40 mét vuông','<p>- KH&ocirc;ng gian y&ecirc;n tĩn</p>\n\n<p>- M&aacute;t mẻ</p>\n\n<p>- Ph&ugrave; hợp v&oacute;i d&acirc;n văn ph&ograve;ng</p>\n',1500000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,40,0,0,'111/6 Đặng Thùy Trâm',10.8292489,106.6173415,1,'2019-12-04 10:27:28.635403','2019-12-04 10:28:07.834977','08d773fa-0f62-f122-f3f8-fda68e243492'),(21,'Phòng trọ cho thuê',1,1,9,8,4,'Nhà ở Quận Thủ Đức diện tích 50 mét vuông','<p>- Kh&ocirc;ng c&oacute; g&igrave; để m&ocirc; tả</p>\n\n<p>&nbsp;</p>\n',16000000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,50,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,2,'2019-12-04 10:32:44.302438','2019-12-04 10:32:44.302438','08d773f4-721a-63d5-985d-5c0740864687'),(22,'Phòng trọ cho thuê',1,1,9,8,4,'Nhà ở Quận Thủ Đức diện tích 50 mét vuông','<p>- Kh&ocirc;ng c&oacute; g&igrave; để m&ocirc; tả</p>\n\n<p>&nbsp;</p>\n',16000000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,50,0,0,'01 Võ Văn Ngân',10.8497386,106.7685109,1,'2019-12-04 10:36:18.855987','2019-12-04 10:36:56.167744','08d773f4-721a-63d5-985d-5c0740864687'),(23,'Phòng trọ cho thuê',1,1,9,8,4,'Nhà ở Quận Thủ Đức diện tích 50 mét vuông','<p>- Kh&ocirc;ng c&oacute; g&igrave; để m&ocirc; tả</p>\n\n<p>&nbsp;</p>\n',16000000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,50,0,0,'Đại học sư phạm kỹ thuật ',10.8497386,106.7685109,1,'2019-12-04 10:38:10.239685','2019-12-04 10:38:19.994637','08d773f4-721a-63d5-985d-5c0740864687'),(24,'Phòng trọ cho thuê',1,1,9,8,4,'Nhà ở Quận Bình Thạnh diện tích 50 mét vuông','<p>- Kh&ocirc;ng c&oacute; g&igrave; để m&ocirc; tả</p>\n\n<p>&nbsp;</p>\n',16000000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,50,0,0,'111 Đặng Thùy Trâm',10.851175,106.7721949,1,'2019-12-04 10:42:46.967540','2019-12-04 10:43:01.067805','08d773f4-721a-63d5-985d-5c0740864687'),(25,'Phòng trọ cho thuê',1,1,9,8,4,'Nhà ở Quận Bình Thạnh diện tích 50 mét vuông','<p>- Kh&ocirc;ng c&oacute; g&igrave; để m&ocirc; tả</p>\n\n<p>&nbsp;</p>\n',16000000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,50,0,0,'111 Đặng Thùy Trâm',10.8292489,106.7026177,2,'2019-12-04 10:45:16.136507','2019-12-04 10:45:16.136508','08d773f4-721a-63d5-985d-5c0740864687'),(26,'Phòng trọ cho thuê',1,1,9,8,4,'Nhà ở Quận Bình Thạnh diện tích 50 mét vuông','<p>- Kh&ocirc;ng c&oacute; g&igrave; để m&ocirc; tả</p>\n\n<p>&nbsp;</p>\n',16000000.000000000000000000000000000000,0.000000000000000000000000000000,0.000000000000000000000000000000,50,0,0,'111 Đặng Thùy Trâm',10.8292489,106.7026177,1,'2019-12-04 10:45:19.907868','2019-12-04 10:45:36.042944','08d773f4-721a-63d5-985d-5c0740864687'),(27,'Cần tìm phòng ở ghép',1,1,1,14,1,'Nhà ở Quận 1 diện tích từ 20 đến 50 mét vuông','<p>- Y&ecirc;u cầu sạch sẽ</p>\n\n<p>rộng r&atilde;i</p>\n\n<p>thoải m&aacute;i thời gian</p>\n\n<p><em><strong><img alt=\"\" src=\"/uploaded/images/20191204/img_1.jpg\" style=\"height:500px; width:500px\" /></strong></em></p>\n',0.000000000000000000000000000000,4000000.000000000000000000000000000000,6000000.000000000000000000000000000000,0,20,50,NULL,10.8230989,106.6173415,2,'2019-12-04 11:39:29.756325','2019-12-04 11:39:29.756370','08d773fa-0f62-f122-f3f8-fda68e243492'),(28,'Cần tìm phòng ở ghép',1,1,1,14,1,'Nhà ở Quận 1 diện tích từ 20 đến 50 mét vuông','<p>- Y&ecirc;u cầu sạch sẽ</p>\n\n<p>rộng r&atilde;i</p>\n\n<p>thoải m&aacute;i thời gian</p>\n\n<p><em><strong><img alt=\"\" src=\"/uploaded/images/20191204/img_1.jpg\" style=\"height:500px; width:500px\" /></strong></em></p>\n',0.000000000000000000000000000000,4000000.000000000000000000000000000000,6000000.000000000000000000000000000000,0,20,50,'111 Đặng Thùy Trâm',10.8230989,106.6296638,1,'2019-12-04 11:40:04.311725','2019-12-04 11:40:24.655640','08d773fa-0f62-f122-f3f8-fda68e243492');
/*!40000 ALTER TABLE `Properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PropertyCategories`
--

DROP TABLE IF EXISTS `PropertyCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PropertyCategories` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Status` int(11) NOT NULL,
  `SortOrder` int(11) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PropertyCategories`
--

LOCK TABLES `PropertyCategories` WRITE;
/*!40000 ALTER TABLE `PropertyCategories` DISABLE KEYS */;
INSERT INTO `PropertyCategories` VALUES (1,'Nhà ở','Nhà nguyên căn, mặt đất',1,1,'2019-11-28 18:16:42.483561','2019-11-28 18:16:42.483667'),(2,'Chung cư','Chung cư, cao ráo, thoáng mát',1,2,'2019-11-28 18:16:42.483751','2019-11-28 18:16:42.483760'),(3,'Phòng trọ','Giá cả vừa phải',1,3,'2019-11-28 18:16:42.483762','2019-11-28 18:16:42.483763');
/*!40000 ALTER TABLE `PropertyCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PropertyImages`
--

DROP TABLE IF EXISTS `PropertyImages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `PropertyImages` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Url` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `PropertyId` int(11) NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_PropertyImages_PropertyId` (`PropertyId`),
  CONSTRAINT `FK_PropertyImages_Properties_PropertyId` FOREIGN KEY (`PropertyId`) REFERENCES `Properties` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PropertyImages`
--

LOCK TABLES `PropertyImages` WRITE;
/*!40000 ALTER TABLE `PropertyImages` DISABLE KEYS */;
INSERT INTO `PropertyImages` VALUES (1,'/uploaded/images/20191129/easy2getroom-herobg3.jpg',16,'2019-11-29 02:26:19.271574','2019-11-29 02:26:19.271922'),(2,'/uploaded/images/20191129/easy2getroom-img1.jpg',16,'2019-11-29 02:26:19.272092','2019-11-29 02:26:19.272119'),(3,'/uploaded/images/20191129/easy2getroom-img2.jpg',16,'2019-11-29 02:26:19.272128','2019-11-29 02:26:19.272128'),(4,'/uploaded/images/20191201/easy2getroom-herobg4.jpg',17,'2019-12-01 11:52:01.386613','2019-12-01 11:52:01.386782'),(5,'/uploaded/images/20191201/easy2getroom-img1.jpg',17,'2019-12-01 11:52:01.386972','2019-12-01 11:52:01.387012'),(6,'/uploaded/images/20191201/easy2getroom-img2.jpg',17,'2019-12-01 11:52:01.387020','2019-12-01 11:52:01.387020'),(7,'/uploaded/images/20191202/easy2getroom-img4.jpg',18,'2019-12-02 22:23:27.826755','2019-12-02 22:23:27.826944'),(8,'/uploaded/images/20191202/easy2getroom-img7.jpg',18,'2019-12-02 22:23:27.827099','2019-12-02 22:23:27.827127'),(9,'/uploaded/images/20191202/easy2getroom-img8.jpg',18,'2019-12-02 22:23:27.827136','2019-12-02 22:23:27.827136'),(10,'/uploaded/images/20191202/easy2getroom-img2.jpg',19,'2019-12-02 22:26:39.583209','2019-12-02 22:26:39.583210'),(11,'/uploaded/images/20191202/easy2getroom-img5.jpg',19,'2019-12-02 22:26:39.583211','2019-12-02 22:26:39.583211'),(12,'/uploaded/images/20191202/easy2getroom-img6.jpg',19,'2019-12-02 22:26:39.583211','2019-12-02 22:26:39.583211'),(13,'/uploaded/images/20191204/easy2getroom-herobg4.jpg',20,'2019-12-04 10:27:28.906061','2019-12-04 10:27:28.906230'),(14,'/uploaded/images/20191204/easy2getroom-img1.jpg',20,'2019-12-04 10:27:28.906320','2019-12-04 10:27:28.906330'),(15,'/uploaded/images/20191204/easy2getroom-img2.jpg',20,'2019-12-04 10:27:28.906333','2019-12-04 10:27:28.906334'),(16,'/uploaded/images/20191204/easy2getroom-img3.jpg',21,'2019-12-04 10:32:55.017367','2019-12-04 10:32:55.017368'),(17,'/uploaded/images/20191204/easy2getroom-img7.jpg',21,'2019-12-04 10:32:55.017369','2019-12-04 10:32:55.017369'),(18,'/uploaded/images/20191204/easy2getroom-img8.jpg',21,'2019-12-04 10:32:55.017369','2019-12-04 10:32:55.017369'),(19,'/uploaded/images/20191204/easy2getroom-img3.jpg',22,'2019-12-04 10:36:29.729145','2019-12-04 10:36:29.729147'),(20,'/uploaded/images/20191204/easy2getroom-img7.jpg',22,'2019-12-04 10:36:29.729148','2019-12-04 10:36:29.729148'),(21,'/uploaded/images/20191204/easy2getroom-img8.jpg',22,'2019-12-04 10:36:29.729149','2019-12-04 10:36:29.729149'),(22,'/uploaded/images/20191204/easy2getroom-img3.jpg',23,'2019-12-04 10:38:10.306164','2019-12-04 10:38:10.306165'),(23,'/uploaded/images/20191204/easy2getroom-img7.jpg',23,'2019-12-04 10:38:10.306166','2019-12-04 10:38:10.306166'),(24,'/uploaded/images/20191204/easy2getroom-img8.jpg',23,'2019-12-04 10:38:10.306167','2019-12-04 10:38:10.306167'),(25,'/uploaded/images/20191204/easy2getroom-img3.jpg',24,'2019-12-04 10:42:47.050228','2019-12-04 10:42:47.050229'),(26,'/uploaded/images/20191204/easy2getroom-img7.jpg',24,'2019-12-04 10:42:47.050230','2019-12-04 10:42:47.050230'),(27,'/uploaded/images/20191204/easy2getroom-img8.jpg',24,'2019-12-04 10:42:47.050230','2019-12-04 10:42:47.050231'),(28,'/uploaded/images/20191204/easy2getroom-img3.jpg',25,'2019-12-04 10:45:20.049796','2019-12-04 10:45:20.049797'),(29,'/uploaded/images/20191204/easy2getroom-img7.jpg',25,'2019-12-04 10:45:20.049798','2019-12-04 10:45:20.049798'),(30,'/uploaded/images/20191204/easy2getroom-img8.jpg',25,'2019-12-04 10:45:20.049798','2019-12-04 10:45:20.049798'),(31,'/uploaded/images/20191204/easy2getroom-img3.jpg',26,'2019-12-04 10:45:20.201845','2019-12-04 10:45:20.201846'),(32,'/uploaded/images/20191204/easy2getroom-img7.jpg',26,'2019-12-04 10:45:20.201847','2019-12-04 10:45:20.201847'),(33,'/uploaded/images/20191204/easy2getroom-img8.jpg',26,'2019-12-04 10:45:20.201847','2019-12-04 10:45:20.201847'),(34,'/uploaded/images/20191204/easy2getroom-herobg4.jpg',27,'2019-12-04 11:39:29.961806','2019-12-04 11:39:29.961931'),(35,'/uploaded/images/20191204/easy2getroom-img1.jpg',27,'2019-12-04 11:39:29.962027','2019-12-04 11:39:29.962039'),(36,'/uploaded/images/20191204/easy2getroom-img2.jpg',27,'2019-12-04 11:39:29.962043','2019-12-04 11:39:29.962043'),(37,'/uploaded/images/20191204/easy2getroom-img1.jpg',27,'2019-12-04 11:39:29.962043','2019-12-04 11:39:29.962043'),(38,'/uploaded/images/20191204/easy2getroom-img2.jpg',27,'2019-12-04 11:39:29.962043','2019-12-04 11:39:29.962044'),(39,'/uploaded/images/20191204/easy2getroom-herobg4.jpg',28,'2019-12-04 11:40:04.391131','2019-12-04 11:40:04.391133'),(40,'/uploaded/images/20191204/easy2getroom-img1.jpg',28,'2019-12-04 11:40:04.391134','2019-12-04 11:40:04.391134'),(41,'/uploaded/images/20191204/easy2getroom-img2.jpg',28,'2019-12-04 11:40:04.391134','2019-12-04 11:40:04.391134'),(42,'/uploaded/images/20191204/easy2getroom-img1.jpg',28,'2019-12-04 11:40:04.391135','2019-12-04 11:40:04.391135'),(43,'/uploaded/images/20191204/easy2getroom-img2.jpg',28,'2019-12-04 11:40:04.391135','2019-12-04 11:40:04.391135');
/*!40000 ALTER TABLE `PropertyImages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RentalTypes`
--

DROP TABLE IF EXISTS `RentalTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `RentalTypes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RentalTypes`
--

LOCK TABLES `RentalTypes` WRITE;
/*!40000 ALTER TABLE `RentalTypes` DISABLE KEYS */;
INSERT INTO `RentalTypes` VALUES (1,'Cần ở ghép','Đăng bài nhằm mục đích cần ở ghép cùng','2019-11-28 18:16:42.588310','2019-11-28 18:16:42.588310'),(2,'Cho ở ghép','Đăng bài nhằm mục đích cho ở ghép cùng','2019-11-28 18:16:42.588310','2019-11-28 18:16:42.588310'),(3,'Cần thuê','Đăng bài nhằm mục đích tìm thuê tài sản','2019-11-28 18:16:42.588300','2019-11-28 18:16:42.588307'),(4,'Cho thuê','Đăng bài nhằm mục đích cho thuê tài sản','2019-11-28 18:16:42.588111','2019-11-28 18:16:42.588216');
/*!40000 ALTER TABLE `RentalTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Roles`
--

DROP TABLE IF EXISTS `Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Roles` (
  `Id` char(36) NOT NULL,
  `Name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `NormalizedName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `ConcurrencyStamp` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles`
--

LOCK TABLES `Roles` WRITE;
/*!40000 ALTER TABLE `Roles` DISABLE KEYS */;
INSERT INTO `Roles` VALUES ('08d773f4-71cb-576f-734c-eddeb8fcba36','Admin','ADMIN','7d5d0222-17ec-4e96-bffe-d7ea0a67e7eb','Quản Trị Hệ Thống'),('08d773f4-71ee-8e52-efed-f896f05a42b4','Moderator','MODERATOR','ab50de7d-1e0d-4694-9504-4a6030bdbcc1','Duyệt Bài Viết'),('08d773f4-71f7-dc88-19fb-432b5c219b7d','Supporter','SUPPORTER','eb61618c-e9c2-4e76-be40-f2a3d283cb13','Hỗ Trợ Người Dùng'),('08d773f4-71fc-cd25-04d0-631ac728a540','User','USER','4c875ff1-85e4-4e5f-b6b3-860d7c0c3ebc','Người Dùng');
/*!40000 ALTER TABLE `Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Users` (
  `Id` char(36) NOT NULL,
  `UserName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `NormalizedUserName` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `Email` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `NormalizedEmail` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `EmailConfirmed` bit(1) NOT NULL,
  `PasswordHash` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `SecurityStamp` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `ConcurrencyStamp` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `PhoneNumber` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `PhoneNumberConfirmed` bit(1) NOT NULL,
  `TwoFactorEnabled` bit(1) NOT NULL,
  `LockoutEnd` datetime(6) DEFAULT NULL,
  `LockoutEnabled` bit(1) NOT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `FullName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `UserEmail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `BirthDay` datetime(6) DEFAULT NULL,
  `Avatar` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `DateCreated` datetime(6) NOT NULL,
  `DateModified` datetime(6) NOT NULL,
  `Status` int(11) NOT NULL,
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES ('08d773f4-721a-63d5-985d-5c0740864687','admin','ADMIN','admin@gmail.com','ADMIN@GMAIL.COM',_binary '\0','AQAAAAEAACcQAAAAECAFbvBuqUQ1eiWWwxhqgXx2EUCFtDz+IMjzgIf6idByLMkKs0Up2GwiaOynq+vhxA==','BLH7YG3TC2XF6K4IIOKXGPDZPPEYSPE5','f3bd5391-0a48-4c51-9708-b6b3c4636a85','0348621188',_binary '\0',_binary '\0',NULL,_binary '',0,'Đinh Quang Nam',NULL,NULL,NULL,'2019-11-28 18:16:41.812898','2019-11-28 18:16:41.813044',0,NULL),('08d773fa-0f62-f122-f3f8-fda68e243492','quangnamute.98@gmail.com','QUANGNAMUTE.98@GMAIL.COM','quangnamute.98@gmail.com','QUANGNAMUTE.98@GMAIL.COM',_binary '\0',NULL,'VXQ5DCASVO6ASN5PS2TWX75O6A3DOT3C','5f0d8fb6-1025-48c0-9ed3-762d043ed53b','0348621188',_binary '\0',_binary '\0',NULL,_binary '',0,'Đinh Quang Nam',NULL,NULL,NULL,'0001-01-01 00:00:00.000000','0001-01-01 00:00:00.000000',0,NULL),('08d773fe-0b8d-981a-9a0b-6784d48c81ed','namnautilus@gmail.com','NAMNAUTILUS@GMAIL.COM','namnautilus@gmail.com','NAMNAUTILUS@GMAIL.COM',_binary '\0',NULL,'UOGVDWWE42JCSPWPZTWQQMOZFMJXPM3B','ad6da95b-533e-4998-8bb8-a43cd4be89d7','0348621188',_binary '\0',_binary '\0',NULL,_binary '',0,'Đinh Thị Nam',NULL,NULL,NULL,'0001-01-01 00:00:00.000000','0001-01-01 00:00:00.000000',0,NULL);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Wards`
--

DROP TABLE IF EXISTS `Wards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Wards` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DistrictId` int(11) NOT NULL,
  `Name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_Wards_DistrictId` (`DistrictId`),
  CONSTRAINT `FK_Wards_Districts_DistrictId` FOREIGN KEY (`DistrictId`) REFERENCES `Districts` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Wards`
--

LOCK TABLES `Wards` WRITE;
/*!40000 ALTER TABLE `Wards` DISABLE KEYS */;
INSERT INTO `Wards` VALUES (1,1,'Phường Tân Định'),(2,9,'Phường Linh Tây'),(3,9,'Phường Linh Chiểu'),(4,9,'Phường Hiệp Bình Chánh'),(5,9,'Phường Hiệp Bình Phước'),(6,9,'Phường Tam Phú'),(7,9,'Phường Tam Bình'),(8,9,'Phường Linh Đông'),(9,9,'Phường Bình Chiểu'),(10,1,'Phường Cầu Ông Lãnh'),(11,1,'Phường Phạm Ngũ Lão'),(12,1,'Phường Nguyễn Thái Bình'),(13,1,'Phường Bến Thành'),(14,1,'Phường Bến Nghé'),(15,1,'Phường Đa Kao'),(16,9,'Phường Linh Xuân'),(17,9,'Phường Trường Thọ');
/*!40000 ALTER TABLE `Wards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `__EFMigrationsHistory`
--

DROP TABLE IF EXISTS `__EFMigrationsHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__EFMigrationsHistory`
--

LOCK TABLES `__EFMigrationsHistory` WRITE;
/*!40000 ALTER TABLE `__EFMigrationsHistory` DISABLE KEYS */;
INSERT INTO `__EFMigrationsHistory` VALUES ('20191123104405_initial','2.2.6-servicing-10079'),('20191126095830_inital-01','2.2.6-servicing-10079'),('20191127183357_initial-02','2.2.6-servicing-10079'),('20191128085811_initial-03','2.2.6-servicing-10079'),('20191128085921_initial-04','2.2.6-servicing-10079');
/*!40000 ALTER TABLE `__EFMigrationsHistory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-04 13:09:31
