﻿var managePostController = function () {
    this.initialize = function () {
        registerEvent();
    }

    var common = {
        activeTab: 1,
        inActiveTab: 2,
        awaitingApprovalTab: 3
    }

    function registerEvent() {

        $('#nav-active-tab').on('click', function () {
            loadActivePost();
        });

        $('#nav-inactive-tab').on('click', function () {
            loadInActivePost();
        });

        $('body').on('click', '.btn-hide', function (e) {
            e.preventDefault();
            var that = $(this).data('id');
            console.log(that);
            easy2getroom.confirm('Bạn có chắc chắn muốn ẩn ?', function () {
                $.ajax({
                    type: 'POST',
                    url: '/ManagePost/Hide',
                    data: {
                        Id: that,
                    },
                    dataType: 'text',
                    beforeSend: function () {
                        easy2getroom.startLoading();
                    },
                    success: function () {
                        easy2getroom.notify('Ẩn thành công', 'success');
                        easy2getroom.stopLoading();

                        loadActivePost();
                    },
                    error: function (status) {
                        console.log(status);
                        easy2getroom.notify('Ẩn không thành công', 'error');
                        easy2getroom.stopLoading();
                    }
                })
            })

        });
        $('body').on('click', '.btn-detail', function (e) {
            e.preventDefault();
            var that = $(this).data('id');

            loadDetail(that);
        });

        $('body').on('click', '.btn-show', function (e) {
            e.preventDefault();
            var that = $(this).data('id');
            console.log(that);
            easy2getroom.confirm('Bạn có chắc chắn muốn bỏ ẩn ?', function () {
                $.ajax({
                    type: 'POST',
                    url: '/ManagePost/Show',
                    data: {
                        Id: that,
                    },
                    dataType: 'text',
                    beforeSend: function () {
                        easy2getroom.startLoading();
                    },
                    success: function () {
                        easy2getroom.notify('Bỏ ẩn thành công', 'success');
                        easy2getroom.stopLoading();

                        loadInActivePost();
                    },
                    error: function (status) {
                        easy2getroom.notify('Bỏ ẩn không thành công', 'error');
                        easy2getroom.stopLoading();
                    }
                })
            })

        });
        $('body').on('click', '.btn-delete', function (e) {
            e.preventDefault();
            var that = $(this).data('id');
            var tab = $(this).data('tab');
            console.log(that);

            easy2getroom.confirm('Bạn có chắc chắn muốn xóa ?', function () {
                $.ajax({
                    type: 'POST',
                    url: '/ManagePost/Delete',
                    data: {
                        id: that,
                    },
                    dataType: 'text',
                    beforeSend: function () {
                        easy2getroom.startLoading();
                    },
                    success: function () {
                        easy2getroom.notify('Xóa thành công', 'success');
                        easy2getroom.stopLoading();
                        if (tab == 1) {
                            loadActivePost();
                        }
                        if (tab == 2) {
                            loadInActivePost();
                        }
                        if (tab == 3) {
                            loadAwaitingApprovalPost();
                        }
                    },
                    error: function (status) {
                        easy2getroom.notify('Xóa không thành công', 'error');
                        easy2getroom.stopLoading();
                    }
                });
            });

            //$('#delete-modal').modal('show');
        });
    }

    function loadActivePost() {
        var template = $('#table-actice-template').html();
        var render = "";
        $.ajax({
            type: 'post',
            url: '/ManagePost/GetAllActivePropertyPaging',
            data: {

            },
            dataType: 'JSON',
            success: function (response) {
                $.each(response.ActivePost, function (i, item) {
                    render += Mustache.render(template, {
                        Id: item.Id,
                        Title: item.Title,
                    });
                });
                $('#tblActive').html(render);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    function loadInActivePost() {
        var template = $('#table-inactice-template').html();
        var render = "";
        $.ajax({
            type: 'post',
            url: '/ManagePost/GetAllInActivePropertyPaging',
            data: {

            },
            dataType: 'JSON',
            success: function (response) {
                console.log(response);
                $.each(response.InActivePost, function (i, item) {
                    render += Mustache.render(template, {
                        Id: item.Id,
                        Title: item.Title,
                    });
                });
                $('#tblInActive').html(render);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function loadAwaitingApprovalPost() {
        var template = $('#table-awaitingapproval-template').html();
        var render = "";
        $.ajax({
            type: 'post',
            url: '/ManagePost/GetAllAwaitingApprovalPropertyPaging',
            data: {

            },
            dataType: 'JSON',
            success: function (response) {
                $.each(response.InActivePost, function (i, item) {
                    render += Mustache.render(template, {
                        Id: item.Id,
                        Title: item.Title,
                    });
                });
                $('#tblAwaitingApproval').html(render);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
}