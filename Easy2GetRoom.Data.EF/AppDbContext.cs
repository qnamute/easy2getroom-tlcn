﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using System;
using System.IO;
using System.Linq;

namespace Easy2GetRoom.Data.EF
{
    public class AppDbContext : IdentityDbContext<User, Role, Guid>
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Announcement> Announcements { get; set; }

        public DbSet<AnnouncementUser> AnnouncementUsers { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<City> Citis { get; set; }

        public DbSet<District> Districts { get; set; }

        public DbSet<Function> Functions { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<MessageImage> MessageImages { get; set; }

        public DbSet<News> News { get; set; }

        public DbSet<NewsImage> NewsImages { get; set; }

        public DbSet<Property> Properties { get; set; }

        public DbSet<PropertyCategory> PropertyCategoris { get; set; }

        public DbSet<PropertyImage> PropertyImages { get; set; }

        public DbSet<RentalType> RentalTypes { get; set; }

        public DbSet<Wards> Wards { get; set; }

        public DbSet<FlaggedProperty> FlaggedProperties { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            #region Identity Config

            builder.Entity<IdentityUserClaim<Guid>>().ToTable("AppUserClaims").HasKey(x => x.Id);
            builder.Entity<IdentityRoleClaim<Guid>>().ToTable("AppRoleClaims")
                .HasKey(x => x.Id);
            builder.Entity<IdentityUserLogin<Guid>>().ToTable("AppUserLogins").HasKey(x => x.UserId);
            builder.Entity<IdentityUserRole<Guid>>().ToTable("AppUserRoles")
                .HasKey(x => new { x.RoleId, x.UserId });
            builder.Entity<IdentityUserToken<Guid>>().ToTable("AppUserTokens")
               .HasKey(x => new { x.UserId });


            // Entity Config


            builder.Entity<Property>()
                .Property(b => b.SlideFlag)
                .HasDefaultValue(0);


            #endregion Identity Config

            //base.OnModelCreating(builder);
            
        }

        public override int SaveChanges()
        {
            //Auto check modified entity and get date created or date modified
            var modified = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified || e.State == EntityState.Added);
            foreach (EntityEntry item in modified)
            {
                var changedOrAddedItem = item.Entity as IDateTracking;
                if (changedOrAddedItem != null)
                {
                    if (item.State == EntityState.Added)
                    {
                        changedOrAddedItem.DateCreated = DateTime.Now;
                    }
                    changedOrAddedItem.DateModified = DateTime.Now;
                }
            }
            return base.SaveChanges();
        }
    }

    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json").Build();

            var builder = new DbContextOptionsBuilder<AppDbContext>();
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            builder.UseMySql(connectionString, mysqlOptions =>
            {
                mysqlOptions
                    .ServerVersion(new Version(8, 0, 18), ServerType.MySql)
                    .CharSetBehavior(CharSetBehavior.AppendToAllColumns)
                    .AnsiCharSet(CharSet.Latin1)
                    .UnicodeCharSet(CharSet.Utf8mb4);
            });
            return new AppDbContext(builder.Options);
        }
    }
}