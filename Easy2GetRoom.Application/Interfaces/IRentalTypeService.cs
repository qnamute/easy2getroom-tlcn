﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Utilities.Dtos;
using System;
using System.Collections.Generic;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IRentalTypeService : IDisposable
    {
        List<RentalTypeViewModel> GetAll();

        RentalTypeViewModel GetById(int id);

        void UpdateStatus(int id, Status status);

        void Delete(int id);

        void Update(RentalTypeViewModel RentalTypeViewModel);

        void Save();
    }
}