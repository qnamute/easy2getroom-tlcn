﻿using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.Interfaces
{
    public interface IWardsService : IDisposable
    {
        List<WardsViewModel> GetAll();

        WardsViewModel GetById(int id);

        void UpdateStatus(int id, Status status);

        void Delete(int id);

        void Update(WardsViewModel wardsViewModel);

        void Save();

        List<WardsViewModel> GetWardsByDistrictId(int id);
    }
}
