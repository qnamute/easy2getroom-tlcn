﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Data.Interfaces
{
    public interface IHasOwner<T>
    {
        T OwnerId { get; set; }
    }
}
