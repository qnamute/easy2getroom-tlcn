﻿var commonconstants = {
    status: {
        active: 1,
        inactive: 0,
        awaitingApproval: 2
    },
    rentalType: {
        needStayWith: 1,
        forStayWith: 2,
        needRent: 3,
        forRent: 4
    }
}