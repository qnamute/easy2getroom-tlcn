﻿using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("Announcements")]
    public class Announcement : DomainEntity<int>, ISwitchable, IDateTracking
    {
        public Announcement()
        {
            AnnouncementUsers = new HashSet<AnnouncementUser>();
        }

        public Announcement(string title, string content, int userId, Status status, DateTime dateCreated, DateTime dateModified)
        {
            Title = title;
            Content = content;
            UserId = userId;
            Status = status;
            DateCreated = dateCreated;
            DateModified = dateModified;
        }

        public string Title { get; set; }

        public string Content { get; set; }

        public int UserId { get; set; }

        public Status Status { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }

        public ICollection<AnnouncementUser> AnnouncementUsers { get; set; }
    }
}
