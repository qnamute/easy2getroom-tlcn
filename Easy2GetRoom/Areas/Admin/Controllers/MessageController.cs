﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Extensions;
using Easy2GetRoom.Infrastructure.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Easy2GetRoom.Areas.Admin.Controllers
{
    public class MessageController : BaseController
    {
        private IMessageService _messageService;
        private IUnitOfWork _unitOfWork;

        public MessageController(IMessageService messageService, IUnitOfWork unitOfWork)
        {
            _messageService = messageService;
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region AJAX API

        public IActionResult GetAllPaging(ReplyMessage? isReply, int page, int pageSize)
        {
            var model = _messageService.GetAllPaging(isReply, page, pageSize);
            return new OkObjectResult(model);
        }

        public IActionResult GetById(int id)
        {
            var model = _messageService.GetById(id);
            return new OkObjectResult(model);
        }

        public IActionResult SaveEntity(MessageViewModel messageViewModel)
        {
            if (User.Identity.IsAuthenticated)
            {
                messageViewModel.IdSender = Guid.Parse(User.GetSpecificClaim("Id"));
                var model = _messageService.Add(messageViewModel);
                return new OkObjectResult(model);
            }
            return new BadRequestResult();
        }

        public IActionResult UpdateStatus(int id)
        {
            _messageService.UpdateStatus(id, ReplyMessage.Replied);
            _unitOfWork.Commit();
            return new OkResult();
        }

        public IActionResult Delete(int id)
        {
            _messageService.Delete(id);
            _unitOfWork.Commit();
            return new OkResult();
        }
        #endregion
    }
}