﻿using AutoMapper.QueryableExtensions;
using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.IRepositories;
using Easy2GetRoom.Infrastructure.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Easy2GetRoom.Application.Implementations
{
    public class PropertyImageService : IPropertyImageService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IPropertyImageRepository _propertyImageRepository;

        public PropertyImageService(IUnitOfWork unitOfWork, IPropertyImageRepository propertyImageRepository)
        {
            _unitOfWork = unitOfWork;
            _propertyImageRepository = propertyImageRepository;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public List<PropertyImageViewModel> GetPropertyImageByPropertyId(int id)
        {
            var data = _propertyImageRepository.FindAll(x => x.PropertyId == id)
                .ProjectTo<PropertyImageViewModel>().ToList();
            return data;
        }

        public void Save(PropertyImageViewModel propertyImageViewModel)
        {
            throw new NotImplementedException();
        }
    }
}