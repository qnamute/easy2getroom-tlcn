﻿using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.ViewModels
{
    public class UserViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string FullName { get; set; }

        public string UserEmail { get; set; }

        public string Email { get; set; }

        public DateTime? BirthDay { get; set; }

        public string Avatar { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public Status Status { get; set; }

        public string PhoneNumber { get; set; }
    }
}
