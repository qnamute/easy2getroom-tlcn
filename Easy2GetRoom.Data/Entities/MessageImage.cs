﻿using Easy2GetRoom.Data.Interfaces;
using Easy2GetRoom.Infrastructure.ShareKernel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Easy2GetRoom.Data.Entities
{
    [Table("MessageImages")]
    public class MessageImage : DomainEntity<int>, IDateTracking
    {
        public string Url { get; set; }

        public int MessageId { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        [ForeignKey("MessageId")]
        public virtual Message Message { get; set; }
    }
}
