﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.IRepositories;

namespace Easy2GetRoom.Data.EF.Repositories
{
    public class FunctionRepository : EFRepository<Function, int>, IFunctionRepository
    {
        public FunctionRepository(AppDbContext context) : base(context)
        {
        }
    }
}