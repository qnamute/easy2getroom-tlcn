﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Easy2GetRoom.Controllers.Components
{
    public class SlideViewComponent : ViewComponent
    {

        private IPropertyService _propertyService;

        public SlideViewComponent(IPropertyService propertyService)
        {
            _propertyService = propertyService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var properties = new List<PropertyViewModel>();
            properties = _propertyService.GetAllSlideProperty();
            return View(properties);
        }
    }
}