﻿using Easy2GetRoom.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easy2GetRoom.Application.ViewModels
{
    public class MessageViewModel
    {
        public int Id { get; set; }

        public int ParentId { get; set; }

        public Guid IdSender { get; set; }

        public string EmailSender { get; set; }

        public ReplyMessage IsReply { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public UserViewModel UserSender { get; set; }
    }
}
