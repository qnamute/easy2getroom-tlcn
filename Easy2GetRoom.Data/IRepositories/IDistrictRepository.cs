﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Infrastructure.Interfaces;

namespace Easy2GetRoom.Data.IRepositories
{
    public interface IDistrictRepository : IRepository<District, int>
    {
    }
}