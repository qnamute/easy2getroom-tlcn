﻿using Easy2GetRoom.Data.Entities;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Utilities.Constants;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Easy2GetRoom.Data.EF
{
    public class DbInitializer
    {
        private readonly AppDbContext _context;
        private UserManager<User> _userManager;
        private RoleManager<Role> _roleManager;

        public DbInitializer(AppDbContext context, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task Seed()
        {
            if (!_roleManager.Roles.Any())
            {
                var _item1 = new Role()
                {
                    Name = "Admin",
                    Description = "Quản Trị Hệ Thống"
                };
                await _roleManager.CreateAsync(_item1);

                //var _item2 = new Role()
                //{
                //    Name = "Moderator",
                //    Description = "Duyệt Bài Viết"
                //};
                //await _roleManager.CreateAsync(_item2);

                //var _item3 = new Role()
                //{
                //    Name = "Supporter",
                //    Description = "Hỗ Trợ Người Dùng"
                //};
                //await _roleManager.CreateAsync(_item3);

                var _item4 = new Role()
                {
                    Name = "User",
                    Description = "Người Dùng"
                };
                await _roleManager.CreateAsync(_item4);
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };
            if (!_userManager.Users.Any())
            {
                var check = await _userManager.CreateAsync(new User()
                {
                    UserName = "admin",
                    FullName = "Đinh Quang Nam",
                    Email = "admin@gmail.com",
                    Status = Status.Active,
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                }, "123456$");

                if (check.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync("admin"); // tim user admin
                    _userManager.AddToRoleAsync(user, "Admin").Wait(); // add admin vao role admin
                }
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.Functions.Count() == 0)
            {
                _context.Functions.AddRange(new List<Function>()
                {
                      new Function() {Id = CommonConstFunctionId.ManagePosts, Name = "Quản lý bài đăng",ParentId = CommonConstFunctionId.DontHaveParent,SortOrder = 1,Status = Status.Active,URL = "",IconCss = "fa-tasks"  },
                      new Function() {Id = CommonConstFunctionId.AllPost, Name = "Tất cả bài đăng",ParentId = CommonConstFunctionId.ManagePosts,SortOrder = 1,Status = Status.Active,URL = "/Admin/Property",IconCss = "fa-home"  },
                      new Function() {Id = CommonConstFunctionId.Active, Name = "Hoạt động",ParentId = CommonConstFunctionId.ManagePosts,SortOrder = 2,Status = Status.Active,URL = "/Admin/ActiveProperty",IconCss = "fa-home"  },
                      new Function() {Id = CommonConstFunctionId.InActive, Name = "Tạm ẩn",ParentId = CommonConstFunctionId.ManagePosts, SortOrder = 3,Status = Status.Active,URL = "/Admin/InActiveProperty",IconCss = "fa-home"  },
                      new Function() {Id = CommonConstFunctionId.AwaitingApproval, Name = "Chờ phê duyệt",ParentId = CommonConstFunctionId.ManagePosts,SortOrder = 4,Status = Status.Active,URL = "/Admin/AwaitingApprovalProperty",IconCss = "fa-home"  },

                      new Function() {Id = CommonConstFunctionId.Messages, Name = "Liên hệ", ParentId = CommonConstFunctionId.DontHaveParent,SortOrder = 2,Status = Status.Active,URL = "/Admin/Message",IconCss = "fa-comments"  },
                      new Function() {Id = CommonConstFunctionId.News, Name = "Tin tức",ParentId = CommonConstFunctionId.DontHaveParent ,SortOrder = 3,Status = Status.Active,URL = "/Admin/News",IconCss = "fa-paper-plane"  },

                      new Function() {Id = CommonConstFunctionId.ManagePlace ,Name = "Quản lý vị trí",ParentId = CommonConstFunctionId.DontHaveParent, SortOrder = 5,Status = Status.Active,URL = "/Admin/Place",IconCss = "fa-map-marker"  },

                      new Function() {Id = CommonConstFunctionId.ManageUser ,Name = "Quản lý người dùng",ParentId = CommonConstFunctionId.DontHaveParent, SortOrder = 6,Status = Status.Active,URL = "/Admin/UserManage",IconCss = "fa-users"  },
                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.PropertyCategoris.Count() == 0)
            {
                _context.PropertyCategoris.AddRange(new List<PropertyCategory>()
                {
                    new PropertyCategory { Name="Nhà ở", Description="Nhà nguyên căn, mặt đất", Status = Status.Active, SortOrder = 1},
                    new PropertyCategory { Name="Chung cư", Description="Chung cư, cao ráo, thoáng mát", Status = Status.Active, SortOrder = 2},
                    new PropertyCategory { Name="Phòng trọ", Description="Giá cả vừa phải", Status = Status.Active, SortOrder = 3},
                });
            }

            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.PropertyCategoris.Count() == 0)
            {
                _context.PropertyCategoris.AddRange(new List<PropertyCategory>()
                {
                    new PropertyCategory { Name="Nhà ở", Description="Nhà nguyên căn, mặt đất", Status = Status.Active, SortOrder = 1},
                    new PropertyCategory { Name="Chung cư", Description="Chung cư, cao ráo, thoáng mát", Status = Status.Active, SortOrder = 2},
                    new PropertyCategory { Name="Phòng trọ", Description="Giá cả vừa phải", Status = Status.Active, SortOrder = 3},
                });
            }

            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            };

            if (_context.RentalTypes.Count() == 0)
            {
                _context.RentalTypes.AddRange(new List<RentalType>()
                {
                    new RentalType() { Name="Cho thuê", Description="Đăng bài nhằm mục đích cho thuê tài sản"},
                    new RentalType() { Name="Cần thuê", Description="Đăng bài nhằm mục đích tìm thuê tài sản"},
                    new RentalType() { Name="Cho ở ghép", Description="Đăng bài nhằm mục đích cho ở ghép cùng"},
                    new RentalType() { Name="Cần ở ghép", Description="Đăng bài nhằm mục đích cần ở ghép cùng"}
                });
            }

            if (_context.Citis.Count() == 0)
            {
                _context.Citis.AddRange(new List<City>()
                {
                    new City() {Id = 1, Name = "Thành Phố Hồ Chí Minh"},
                    new City() {Id = 2, Name = "Thành Phố Hà Nội"},
                    new City() {Id = 3, Name = "Thành Phố Đà Nẵng"},
                    new City() {Id = 4, Name = "Tỉnh Thừa Thiên Huế"},
                    new City() {Id = 5, Name = "Tỉnh Đăk Nông"},
                    new City() {Id = 6, Name = "Thành Phố Buôn Mê Thuật"},
                    new City() {Id = 7, Name = "Thành Phố Bình Dương"},
                    new City() {Id = 8, Name = "Tỉnh Bình Định"},
                    new City() {Id = 9, Name = "Tỉnh Long An"},
                    new City() {Id = 10, Name = "Tỉnh Thanh Hóa"},
                    new City() {Id = 11, Name = "Tỉnh Nghệ An"},
                    new City() {Id = 12, Name = "Thành Phố Cần Thơ"},
                    new City() {Id = 13, Name = "Tỉnh Đồng Nai"},
                    new City() {Id = 14, Name = "Tỉnh Cà Mau"},
                    new City() {Id = 15, Name = "Tỉnh Thái Bình"},
                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            }

            if (_context.Districts.Count() == 0)
            {
                _context.Districts.AddRange(new List<District>()
                {
                    new District() {Id = 1, CityId = 1, Name = "Quận 1"},
                    new District() {Id = 2, CityId = 1, Name = "Quận 2"},
                    new District() {Id = 3, CityId = 1, Name = "Quận 3"},
                    new District() {Id = 4, CityId = 1, Name = "Quận 4"},
                    new District() {Id = 5, CityId = 1, Name = "Quận 5"},
                    new District() {Id = 6, CityId = 1, Name = "Quận 6"},
                    new District() {Id = 7, CityId = 1, Name = "Quận 7"},
                    new District() {Id = 8, CityId = 1, Name = "Quận 8"},
                    new District() {Id = 9, CityId = 1, Name = "Quận 9"},
                    new District() {Id = 10, CityId = 1, Name = "Quận 10"},
                    new District() {Id = 11, CityId = 1, Name = "Quận 11"},
                    new District() {Id = 12, CityId = 1, Name = "Quận 12"},
                    new District() {Id = 13, CityId = 1, Name = "Quận Bình Thạnh"},
                    new District() {Id = 14, CityId = 1, Name = "Quận Thủ Đức"},
                    new District() {Id = 15, CityId = 1, Name = "Quận Gò Vấp"},
                    new District() {Id = 16, CityId = 1, Name = "Quận Bình Tân"},
                    new District() {Id = 17, CityId = 1, Name = "Quận Tân Bình"},
                    new District() {Id = 18, CityId = 1, Name = "Quận Tân Phú"},
                    new District() {Id = 19, CityId = 1, Name = "Huyện Bình Chánh"},
                    new District() {Id = 20, CityId = 1, Name = "Huyện Nhà Bè"},
                    new District() {Id = 21, CityId = 1, Name = "Huyện Cần Giờ"},
                    new District() {Id = 22, CityId = 1, Name = "Huyện Hóc Môn"},
                    new District() {Id = 23, CityId = 1, Name = "Huyện Củ Chi"},
                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            }

            if (_context.Wards.Count() == 0)
            {
                _context.Wards.AddRange(new List<Wards>()
                {
                    new Wards() { DistrictId = 1, Name = "Phường Tân Định"},
                    new Wards() { DistrictId = 1, Name = "Phường Đa Kao"},
                    new Wards() { DistrictId = 1, Name = "Phường Bến Nghé"},
                    new Wards() { DistrictId = 1, Name = "Phường Bến Thành"},
                    new Wards() { DistrictId = 1, Name = "Phường Nguyễn Thái Bình"},
                    new Wards() { DistrictId = 1, Name = "Phường Phạm Ngũ Lão"},
                    new Wards() { DistrictId = 1, Name = "Phường Cầu Ông Lãnh"},

                    new Wards() { DistrictId = 2, Name = "Phường An Khánh"},
                    new Wards() { DistrictId = 2, Name = "Phường An Lợi Đông"},
                    new Wards() { DistrictId = 2, Name = "Phường An Phú"},
                    new Wards() { DistrictId = 2, Name = "Phường Binh An"},
                    new Wards() { DistrictId = 2, Name = "Phường Bình Khánh"},
                    new Wards() { DistrictId = 2, Name = "Phường Bình Trưng Đông"},
                    new Wards() { DistrictId = 2, Name = "Phường Bình Trưng Tây"},
                    new Wards() { DistrictId = 2, Name = "Phường Cát Lái"},
                    new Wards() { DistrictId = 2, Name = "Phường Thạnh Mỹ Lợi"},
                    new Wards() { DistrictId = 2, Name = "Phường Thảo Điền"},
                    new Wards() { DistrictId = 2, Name = "Phường Thủ Thiêm"},

                    new Wards() { DistrictId = 3, Name = "Phường 1"},
                    new Wards() { DistrictId = 3, Name = "Phường 2"},
                    new Wards() { DistrictId = 3, Name = "Phường 3"},
                    new Wards() { DistrictId = 3, Name = "Phường 4"},
                    new Wards() { DistrictId = 3, Name = "Phường 5"},
                    new Wards() { DistrictId = 3, Name = "Phường 6"},
                    new Wards() { DistrictId = 3, Name = "Phường 7"},
                    new Wards() { DistrictId = 3, Name = "Phường 8"},
                    new Wards() { DistrictId = 3, Name = "Phường 9"},
                    new Wards() { DistrictId = 3, Name = "Phường 10"},
                    new Wards() { DistrictId = 3, Name = "Phường 11"},
                    new Wards() { DistrictId = 3, Name = "Phường 12"},
                    new Wards() { DistrictId = 3, Name = "Phường 13"},
                    new Wards() { DistrictId = 3, Name = "Phường 14"},

                    new Wards() { DistrictId = 4, Name = "Phường 1"},
                    new Wards() { DistrictId = 4, Name = "Phường 2"},
                    new Wards() { DistrictId = 4, Name = "Phường 3"},
                    new Wards() { DistrictId = 4, Name = "Phường 4"},
                    new Wards() { DistrictId = 4, Name = "Phường 5"},
                    new Wards() { DistrictId = 4, Name = "Phường 6"},
                    new Wards() { DistrictId = 4, Name = "Phường 8"},
                    new Wards() { DistrictId = 4, Name = "Phường 9"},
                    new Wards() { DistrictId = 4, Name = "Phường 10"},
                    new Wards() { DistrictId = 4, Name = "Phường 12"},
                    new Wards() { DistrictId = 4, Name = "Phường 13"},
                    new Wards() { DistrictId = 4, Name = "Phường 14"},
                    new Wards() { DistrictId = 4, Name = "Phường 15"},
                    new Wards() { DistrictId = 4, Name = "Phường 16"},
                    new Wards() { DistrictId = 4, Name = "Phường 18"},

                    new Wards() { DistrictId = 5, Name = "Phường 1"},
                    new Wards() { DistrictId = 5, Name = "Phường 2"},
                    new Wards() { DistrictId = 5, Name = "Phường 3"},
                    new Wards() { DistrictId = 5, Name = "Phường 4"},
                    new Wards() { DistrictId = 5, Name = "Phường 5"},
                    new Wards() { DistrictId = 5, Name = "Phường 6"},
                    new Wards() { DistrictId = 5, Name = "Phường 7"},
                    new Wards() { DistrictId = 5, Name = "Phường 8"},
                    new Wards() { DistrictId = 5, Name = "Phường 9"},
                    new Wards() { DistrictId = 5, Name = "Phường 10"},
                    new Wards() { DistrictId = 5, Name = "Phường 11"},
                    new Wards() { DistrictId = 5, Name = "Phường 12"},
                    new Wards() { DistrictId = 5, Name = "Phường 13"},
                    new Wards() { DistrictId = 5, Name = "Phường 14"},
                    new Wards() { DistrictId = 5, Name = "Phường 15"},

                    new Wards() { DistrictId = 6, Name = "Phường 1"},
                    new Wards() { DistrictId = 6, Name = "Phường 2"},
                    new Wards() { DistrictId = 6, Name = "Phường 3"},
                    new Wards() { DistrictId = 6, Name = "Phường 4"},
                    new Wards() { DistrictId = 6, Name = "Phường 5"},
                    new Wards() { DistrictId = 6, Name = "Phường 6"},
                    new Wards() { DistrictId = 6, Name = "Phường 7"},
                    new Wards() { DistrictId = 6, Name = "Phường 8"},
                    new Wards() { DistrictId = 6, Name = "Phường 9"},
                    new Wards() { DistrictId = 6, Name = "Phường 10"},
                    new Wards() { DistrictId = 6, Name = "Phường 11"},
                    new Wards() { DistrictId = 6, Name = "Phường 12"},
                    new Wards() { DistrictId = 6, Name = "Phường 13"},
                    new Wards() { DistrictId = 6, Name = "Phường 14"},

                    new Wards() { DistrictId = 7, Name = "Phường Phú Mỹ"},
                    new Wards() { DistrictId = 7, Name = "Phường Phú Thuận"},
                    new Wards() { DistrictId = 7, Name = "Phường Tân Phú"},
                    new Wards() { DistrictId = 7, Name = "Phường Tân Thuận Đông"},
                    new Wards() { DistrictId = 7, Name = "Phường Bình Thuận"},
                    new Wards() { DistrictId = 7, Name = "Phường Tân Thuận Tây"},
                    new Wards() { DistrictId = 7, Name = "Phường Tân Kiểng"},
                    new Wards() { DistrictId = 7, Name = "Phường Tân Quy"},
                    new Wards() { DistrictId = 7, Name = "Phường Tân Phong"},
                    new Wards() { DistrictId = 7, Name = "Phường Tân Hưng"},

                    new Wards() { DistrictId = 8, Name = "Phường 1"},
                    new Wards() { DistrictId = 8, Name = "Phường 2"},
                    new Wards() { DistrictId = 8, Name = "Phường 3"},
                    new Wards() { DistrictId = 8, Name = "Phường 4"},
                    new Wards() { DistrictId = 8, Name = "Phường 5"},
                    new Wards() { DistrictId = 8, Name = "Phường 6"},
                    new Wards() { DistrictId = 8, Name = "Phường 7"},
                    new Wards() { DistrictId = 8, Name = "Phường 8"},
                    new Wards() { DistrictId = 8, Name = "Phường 9"},
                    new Wards() { DistrictId = 8, Name = "Phường 10"},
                    new Wards() { DistrictId = 8, Name = "Phường 11"},
                    new Wards() { DistrictId = 8, Name = "Phường 12"},
                    new Wards() { DistrictId = 8, Name = "Phường 13"},
                    new Wards() { DistrictId = 8, Name = "Phường 14"},
                    new Wards() { DistrictId = 8, Name = "Phường 15"},
                    new Wards() { DistrictId = 8, Name = "Phường 16"},

                    new Wards() { DistrictId = 9, Name = "Phường Hiệp Phú"},
                    new Wards() { DistrictId = 9, Name = "Phường Long Bình"},
                    new Wards() { DistrictId = 9, Name = "Phường Long Phước"},
                    new Wards() { DistrictId = 9, Name = "Phường Long Thạnh Mỹ"},
                    new Wards() { DistrictId = 9, Name = "Phường Long Trường"},
                    new Wards() { DistrictId = 9, Name = "Phường Phú Hữu"},
                    new Wards() { DistrictId = 9, Name = "Phường Phước Bình"},
                    new Wards() { DistrictId = 9, Name = "Phường Phước Long A"},
                    new Wards() { DistrictId = 9, Name = "Phường Phước Long B"},
                    new Wards() { DistrictId = 9, Name = "Phường Tân Phú"},
                    new Wards() { DistrictId = 9, Name = "Phường Tăng Nhơn Phú A"},
                    new Wards() { DistrictId = 9, Name = "Phường Tăng Nhơn Phú B"},
                    new Wards() { DistrictId = 9, Name = "Phường Trường Thạnh"},

                    new Wards() { DistrictId = 10, Name = "Phường 1"},
                    new Wards() { DistrictId = 10, Name = "Phường 2"},
                    new Wards() { DistrictId = 10, Name = "Phường 3"},
                    new Wards() { DistrictId = 10, Name = "Phường 4"},
                    new Wards() { DistrictId = 10, Name = "Phường 5"},
                    new Wards() { DistrictId = 10, Name = "Phường 6"},
                    new Wards() { DistrictId = 10, Name = "Phường 7"},
                    new Wards() { DistrictId = 10, Name = "Phường 8"},
                    new Wards() { DistrictId = 10, Name = "Phường 9"},
                    new Wards() { DistrictId = 10, Name = "Phường 10"},
                    new Wards() { DistrictId = 10, Name = "Phường 11"},
                    new Wards() { DistrictId = 10, Name = "Phường 12"},
                    new Wards() { DistrictId = 10, Name = "Phường 13"},
                    new Wards() { DistrictId = 10, Name = "Phường 14"},
                    new Wards() { DistrictId = 10, Name = "Phường 15"},

                    new Wards() { DistrictId = 11, Name = "Phường 1"},
                    new Wards() { DistrictId = 11, Name = "Phường 2"},
                    new Wards() { DistrictId = 11, Name = "Phường 3"},
                    new Wards() { DistrictId = 11, Name = "Phường 4"},
                    new Wards() { DistrictId = 11, Name = "Phường 5"},
                    new Wards() { DistrictId = 11, Name = "Phường 6"},
                    new Wards() { DistrictId = 11, Name = "Phường 7"},
                    new Wards() { DistrictId = 11, Name = "Phường 8"},
                    new Wards() { DistrictId = 11, Name = "Phường 9"},
                    new Wards() { DistrictId = 11, Name = "Phường 10"},
                    new Wards() { DistrictId = 11, Name = "Phường 11"},
                    new Wards() { DistrictId = 11, Name = "Phường 12"},
                    new Wards() { DistrictId = 11, Name = "Phường 13"},
                    new Wards() { DistrictId = 11, Name = "Phường 14"},
                    new Wards() { DistrictId = 11, Name = "Phường 15"},
                    new Wards() { DistrictId = 11, Name = "Phường 16"},

                    new Wards() { DistrictId = 12, Name = "Phường An Phú Đông"},
                    new Wards() { DistrictId = 12, Name = "Phường Đông Hưng Thuận"},
                    new Wards() { DistrictId = 12, Name = "Phường Hiệp Thành"},
                    new Wards() { DistrictId = 12, Name = "Phường Tân Chánh Hiệp"},
                    new Wards() { DistrictId = 12, Name = "Phường Tân Hưng Thuận"},
                    new Wards() { DistrictId = 12, Name = "Phường Tân Thới Hiệp"},
                    new Wards() { DistrictId = 12, Name = "Phường Tân Thới Nhất"},
                    new Wards() { DistrictId = 12, Name = "Phường Thạnh Lộc"},
                    new Wards() { DistrictId = 12, Name = "Phường Thạnh Xuân"},
                    new Wards() { DistrictId = 12, Name = "Phường Thới An"},
                    new Wards() { DistrictId = 12, Name = "Phường Trung Mỹ Tây"},

                    new Wards() { DistrictId = 13, Name = "Phường 1"},
                    new Wards() { DistrictId = 13, Name = "Phường 2"},
                    new Wards() { DistrictId = 13, Name = "Phường 3"},
                    new Wards() { DistrictId = 13, Name = "Phường 5"},
                    new Wards() { DistrictId = 13, Name = "Phường 6"},
                    new Wards() { DistrictId = 13, Name = "Phường 7"},
                    new Wards() { DistrictId = 13, Name = "Phường 11"},
                    new Wards() { DistrictId = 13, Name = "Phường 12"},
                    new Wards() { DistrictId = 13, Name = "Phường 13"},
                    new Wards() { DistrictId = 13, Name = "Phường 14"},
                    new Wards() { DistrictId = 13, Name = "Phường 15"},
                    new Wards() { DistrictId = 13, Name = "Phường 17"},
                    new Wards() { DistrictId = 13, Name = "Phường 19"},
                    new Wards() { DistrictId = 13, Name = "Phường 21"},
                    new Wards() { DistrictId = 13, Name = "Phường 22"},
                    new Wards() { DistrictId = 13, Name = "Phường 24"},
                    new Wards() { DistrictId = 13, Name = "Phường 25"},
                    new Wards() { DistrictId = 13, Name = "Phường 26"},
                    new Wards() { DistrictId = 13, Name = "Phường 27"},

                    new Wards() { DistrictId = 14, Name = "Phường Linh Xuân"},
                    new Wards() { DistrictId = 14, Name = "Phường Bình Chiểu"},
                    new Wards() { DistrictId = 14, Name = "Phường Tam Bình"},
                    new Wards() { DistrictId = 14, Name = "Phường Tam Phú"},
                    new Wards() { DistrictId = 14, Name = "Phường Hiệp Bình Phước"},
                    new Wards() { DistrictId = 14, Name = "Phường Hiệp Bình Chánh"},
                    new Wards() { DistrictId = 14, Name = "Phường Linh Chiểu"},
                    new Wards() { DistrictId = 14, Name = "Phường Linh Tây"},
                    new Wards() { DistrictId = 14, Name = "Phường Linh Đông"},
                    new Wards() { DistrictId = 14, Name = "Phường Trường Thọ"},
                });
            }
            try
            {
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                string a = ex.ToString();
            }

            //if (_context.Properties.Count() == 0)
            //{
            //    _context.Properties.AddRange(new List<Property>()
            //        {
            //        new Property() { Name = "Phòng trọ cho thuê", PropertyCategoryId = CommonConstantsProperty.Rooms, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price=2500000, Acreage=25, Address="01 Võ Văn Ngân", Status = Status.Active},
            //        new Property() { Name = "Phòng trọ cho thuê", PropertyCategoryId = CommonConstantsProperty.Rooms, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.Active },
            //        new Property() { Name = "Phòng trọ cho thuê", PropertyCategoryId = CommonConstantsProperty.Rooms, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.Active },
            //        new Property() { Name = "Nhà ở", PropertyCategoryId = CommonConstantsProperty.Houses, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.Active },
            //        new Property() { Name = "Nhà ở", PropertyCategoryId = CommonConstantsProperty.Houses, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.Active },
            //        new Property() { Name = "Nhà ở", PropertyCategoryId = CommonConstantsProperty.Houses, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.Active },
            //        new Property() { Name = "Nhà ở cho thuê", PropertyCategoryId = CommonConstantsProperty.Houses, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.InActive },
            //        new Property() { Name = "Nhà ở cho thuê", PropertyCategoryId = CommonConstantsProperty.Houses, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.InActive },
            //        new Property() { Name = "Chung cư cho thuê", PropertyCategoryId = CommonConstantsProperty.Apartments, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.InActive },
            //        new Property() { Name = "Chung cư cho thuê", PropertyCategoryId = CommonConstantsProperty.Apartments, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.InActive },
            //        new Property() { Name = "Chung cư cho thuê", PropertyCategoryId = CommonConstantsProperty.Apartments, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.InActive },
            //        new Property() { Name = "Phòng trọ cho thuê", PropertyCategoryId = CommonConstantsProperty.Rooms, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.AwaitingApproval },
            //        new Property() { Name = "Phòng trọ cho thuê", PropertyCategoryId = CommonConstantsProperty.Rooms, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.AwaitingApproval },
            //        new Property() { Name = "Phòng trọ cho thuê", PropertyCategoryId = CommonConstantsProperty.Rooms, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.AwaitingApproval },
            //        new Property() { Name = "Chung cư cho thuê", PropertyCategoryId = CommonConstantsProperty.Apartments, RentalTypeId = CommonConstantsProperty.ForRent, CityId = 1,  DistrictId = 14, WardsId = 8, Title = "Phòng trọ thủ đức 20m2", Description = "Phòng trọ Thủ Đức, rộng rãi", Price = 2500000, Acreage = 25, Address = "01 Võ Văn Ngân", Status = Status.AwaitingApproval },
            //    });
            //}

            //try
            //{
            //    _context.SaveChanges();
            //}
            //catch (Exception ex)
            //{
            //    string a = ex.ToString();
            //}
        }
    }
}