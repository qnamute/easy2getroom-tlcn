﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Easy2GetRoom.Application.ViewModels
{
    public class CityViewModel
    {
        public int Id { get; set; }

        [StringLength(255)]
        [Required]
        public string Name { get; set; }

        public List<DistrictViewModel> Districts { get; set; }
    }
}
