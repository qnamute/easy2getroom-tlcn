﻿using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Data.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Easy2GetRoom.Data.Entities
{
    [Table("Users")]
    public class User : IdentityUser<Guid>, IDateTracking, ISwitchable
    {
        public User()
        {
        }

        public User(Guid id)
        {
            Id = id;
        }

        public User(string name, string fullName, string userEmail, DateTime? birthDay, string avatar, DateTime dateCreated, DateTime dateModified, Status status)
        {
            Name = name;
            FullName = fullName;
            UserEmail = userEmail;
            BirthDay = birthDay;
            Avatar = avatar;
            DateCreated = dateCreated;
            DateModified = dateModified;
            Status = status;
        }

        public User(Guid id, string name, string fullName, string userEmail, DateTime? birthDay, string avatar, DateTime dateCreated, DateTime dateModified, Status status)
        {
            Id = id;
            Name = name;
            FullName = fullName;
            UserEmail = userEmail;
            BirthDay = birthDay;
            Avatar = avatar;
            DateCreated = dateCreated;
            DateModified = dateModified;
            Status = status;
        }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string FullName { get; set; }

        [StringLength(255)]
        public string UserEmail { get; set; }

        public DateTime? BirthDay { get; set; }

        public string Avatar { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public Status Status { get; set; }
    }
}