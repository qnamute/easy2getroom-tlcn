﻿using Easy2GetRoom.Application.Interfaces;
using Easy2GetRoom.Application.ViewModels;
using Easy2GetRoom.Data.Enums;
using Easy2GetRoom.Extensions;
using Easy2GetRoom.Models.UpPostViewmodels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Easy2GetRoom.Controllers.UpPostProperty
{
    [Authorize]
    public class UpPostController : Controller
    {
        private IPropertyCategoryService _propertyCategoryService;
        private IRentalTypeService _rentalTypeService;
        private ICityService _cityService;
        private IPropertyImageService _propertyImageService;
        private IPropertyService _propertyService;

        public UpPostController(IPropertyCategoryService propertyCategoryService, IRentalTypeService rentalTypeService,
            ICityService cityService, IPropertyImageService propertyImageService, IPropertyService propertyService)
        {
            _propertyCategoryService = propertyCategoryService;
            _rentalTypeService = rentalTypeService;
            _cityService = cityService;
            _propertyImageService = propertyImageService;
            _propertyService = propertyService;
        }

        [Route("up-post-property.html")]
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                Redirect("/login.html");
            }
            var upPostVM = new UpPostViewModel();

            upPostVM.Categories = _propertyCategoryService.GetAll();
            upPostVM.RentalTypes = _rentalTypeService.GetAll();
            upPostVM.Cities = _cityService.GetAll();

            return View(upPostVM);
        }

        [Route("uppost-success.html")]
        public IActionResult SuccessPageRedirect()
        {
            return View();
        }

        [Route("uppost-failed.html")]
        public IActionResult FailedPageRedirect()
        {
            return View();
        }

        #region AJAX API

        public IActionResult SavePost(PropertyViewModel propertyViewModel)
        {
            if (User.Identity.IsAuthenticated)
            {
                Guid userId = Guid.Parse(User.GetSpecificClaim("Id"));
                propertyViewModel.UserId = userId;
                if (propertyViewModel.Id == 0)
                {
                    propertyViewModel.Status = Status.AwaitingApproval;
                    propertyViewModel = _propertyService.Add(propertyViewModel);
                }
                else
                {
                    _propertyService.Update(propertyViewModel);
                    _propertyService.Save();
                }
                return new OkObjectResult(propertyViewModel);
            }
            else return new BadRequestResult();
        }

        public IActionResult SaveImageProduct(string[] imagesUrl, int propertyId)
        {
            if (User.Identity.IsAuthenticated)
            {
                RedirectToAction("/Login");
            }
            _propertyService.AddImages(imagesUrl, propertyId);
            _propertyService.Save();

            return new OkObjectResult(imagesUrl);
        }

        #endregion
    }
}